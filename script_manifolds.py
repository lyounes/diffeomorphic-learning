from sys import path as sys_path
sys_path.append('..')
sys_path.append('../base')
sys_path.append('../data')
import time
import logging
import matplotlib.pyplot as plt
from base import loggingUtils
from base import pointSets as ps
from datetime import timedelta
import numpy as np
from data import dataSets
from diffeoLearn import DiffeoLearn


#                         NTr, dim
# SwissRoll2D            2000,  2
# SwissRoll3D            5000,  3


# manifold experiments
experiments = []

# experiments += [{'data': 'SwissRoll2D',         'NTr': 700,    'NTe': 2000,    'nRuns': 1,     'case': 0}]
# experiments += [{'data': 'SwissRoll3D',         'NTr': 4000,    'NTe': 3000,    'nRuns': 1,     'case': 0}]
experiments += [{'data': 'S_Curve',         'NTr': 4000,    'NTe': 2000,    'nRuns': 1,     'case': 0}]


for expt in experiments:

    typeData0 = expt['data']
    nRuns = expt['nRuns']
    case = expt['case']
    NTr = expt.get('NTr')
    NTe = expt.get('NTe')

    if case == 0: params = {'approach':1,  # 0 = standard FineMorphs, 1 = one patch added at a time, 2 = linear model
                            'clamp': [0],  # only used for approach 1 or 2 (approach 2 modifies this term)
                            'C': 10000.,  # 10000.,  # clamping penalty
                            # 'freezeLayer':[],
                            'layers':['D','D'],
                            'gradEps':1e-12,
                            'objEps':1e-12,
                            'maxIter':500, #1000,
                            'layerOutDim':[None], 'addDim_Y':0, 'addDim_X':2,
                            'XTr_initFactor':0.0,
                            'YTr_initFactor':0.01,
                            # 'b_flag':[False],
                            # 'ridgeType':['diag'],
                            # 'affine':['euclidean','none'],
                            # 'kernelSigma':[0.5,0.5], # SwissRoll2D
                            'kernelSigma':[0.4,0.4], # S_Curve
                            'removeDimLayer_In':[[1], [[0,1,2]]], #[[1], [[0,1]]],
                            'addDimLayer_In':[[1], [[-1]]],
                            'sigmaErrorFactor':0.01, #0.01,
                            'update_sigmaError':False,
                            'ball_eps': 0.4, #0.80, # S_Curve
                            # 'ball_eps': 0.5, # SwissRoll2D
                            # 'patchesLayer': [0],
                            # 'repulseLayer_In': [[1,2], ['all','all']],
                            # 'repulseLayer_Out':[[0],[[-2,-1]]],
                            # 'repulse_sigmaErrorFactor':1.0,
                            # 'elasticPatchesLayer_In': [[1], ['all']],
                            # 'elasticPatchesLayer_Out': [[0], ['all']],
                            # 'elasticPatches_sigmaErrorFactor':1.0,
                            # 'zeroFlattenLayer_In': [[1], [[0,1]]],
                            # 'zeroFlattenLayer_Out': [[0], [[0,1,2]]],
                            # 'zeroFlatten_sigmaErrorFactor': .1,
                            # 'flattenLayer_In': [[1], [[0,1]]],
                            # 'flattenLayer_Out':[[0],[[0,1]]],
                            # 'flatten_sigmaErrorFactor':1.,
                            # 'centerLayer_In':[[4], ['all']],
                            # # # 'centerLayer_Out':[[1,2],['all','all']],
                            # 'center_sigmaErrorFactor':.1,
                            # 'stitchLayer_In':[[1], ['all']],
                            # 'stitchLayer_Out':[[0], ['all']],
                            # 'stitch_sigmaErrorFactor':.01,
                            # 'ball_eps_elastic': 0.3,
                            # 'elasticLayer_In': [[1], ['all']],
                            # # 'elasticLayer_Out': [[0], ['all']],
                            # 'elastic_sigmaErrorFactor':1.0
                            }
    elif case == 1: params = {'layers':['D','D','D','D','A','D'],  # 'S_Curve'
                              'gradEps':1e-12,
                              'objEps':1e-12,
                              'maxIter':4000,
                              'layerOutDim':[None],
                              'addDim_X':2, 'XTr_initFactor':0.01,
                              # 'applyInitDef': True, 'addDim_initDef': 1,
                              'addDim_Y':0, 'YTr_initFactor':0.01,
                              'kernelSigma':[0.8,0.6,0.4,0.2,0.5],
                              'removeDimLayer_In':[[4], [[0,1]]],
                              'addDimLayer_In':[[5], [[-1]]],
                              'sigmaErrorFactor':0.1,
                              'update_sigmaError':False,
                              'ball_eps': 0.80,
                              'patchesLayer': [0,1,2,3],
                              # 'repulseLayer_In': [[5], ['all']],
                              # # 'repulseLayer_Out': [[0], [[-2, -1]]],
                              # 'repulse_sigmaErrorFactor': 1.0,
                              # 'elasticPatchesLayer_In': [[5], ['all']],
                              # 'elasticPatches_sigmaErrorFactor':1.0,
                              'stitchLayer_In':[[5], ['all']],
                              'stitch_sigmaErrorFactor':0.1,
                              }
    elif case == 2: params = {'layers':['D','D','D'],
                            # 'trainSubset':[[0,1],['patches','KMeans'],[None,100]],
                            # 'trainSubset': [[2], ['patches'], [None]],
                            'layerOutDim':[None], 'addDim_Y':0, 'addDim_X':1,
                            # 'YTr_initFactor':0.01,
                            'XTr_initFactor':0.0, #0.0001,
                            # 'b_flag':[False,False],
                            # 'ridgeType':['reg','diag'],
                            # 'affine':['euclidean','none'],
                            'removeDimLayer_In':[[1], [[0,1]]],
                            'addDimLayer_In':[[2], [[-1]]],
                            'sigmaErrorFactor':1.,
                            # 'ball_eps': 0.80, # S_Curve
                            'ball_eps': 0.6,
                            'patchesLayer': [0,1],
                            'repulseLayer_In': [[2], ['all']],
                            'repulseLayer_Out':[[1],['all']],
                            'repulse_sigmaErrorFactor':1.,
                            'ball_eps_elastic': 0.3,
                            'elasticLayer_In': [[], []],
                            'elasticLayer_Out': [[], []],
                            'elastic_sigmaErrorFactor':1.,
                            # 'flattenLayer_In': [[1], [[0,1]]],
                            # 'flattenLayer_Out':[[0,1],[[0,1],[0,1]]],
                            # 'flatten_sigmaErrorFactor':1.,
                            'centerLayer_In':[[2], ['all']],
                            'centerLayer_Out':[[1],['all']],
                            'center_sigmaErrorFactor':.1,
                            'stitchLayer_In':[[2], ['all']],
                            'stitchLayer_Out':[[1], ['all']],
                            'stitch_sigmaErrorFactor':1.
                            }


    logging_flag = True
    loggingFile = typeData0+'_case_'+str(case)+'_info'
    base_params = {'dtype':'float32', #'testGradient':True,
                   'logging_flag':logging_flag, 'loggingFile':loggingFile,
                   'plot_flag':True, 'saveFigure_flag':True,
                   # 'saveTraj_flag':True, 'save_layer_D':0
                   }

    parameters = {**params, **base_params}

    train_errors = np.zeros((1, nRuns))
    test_errors = np.zeros((1, nRuns))
    times = np.zeros((1, nRuns))

    pykeops_cleanup = True

    f = DiffeoLearn(parameters)
    for run in range(nRuns):

        # read in data
        data = dataSets.DataConfiguration(typeData0=typeData0, run=run, NTr=NTr, NTe=NTe)
        typeData = typeData0 + '_' + str(run) + '_case_' + str(case)
        print('\n{0:s}\n'.format(typeData))

        # run model
        startTime = time.time()
        f.fit(data.XTr, data.YTr, cTr=data.cTr, typeData=typeData, pykeops_cleanup=pykeops_cleanup,
              standardize_fit_x = True, standardize_fit_y = True)
        train_results = f.predict(data.XTr)
        test_results = f.predict(data.XTe)
        endTime = time.time()

        # calculate errors and times
        train_errors[0, run] = ((train_results - data.YTr) ** 2).sum() / data.YTr.shape[0]
        test_errors[0, run] = ((test_results - data.YTe) ** 2).sum() / data.YTe.shape[0]
        times[0, run] = endTime - startTime
        print('    elapsed time: {:0>8}'.format(str(timedelta(seconds=times[0, run]))))

        # for layer_D in range(f.nD):
        #     fig = plt.figure('Training PCA')
        #     fig.clf()
        #     ax1 = fig.add_subplot(211)
        #     ax2 = fig.add_subplot(212)
        #     if f.zt[layer_D].shape[-1] > 1:
        #         ax1.plot(ps.runPCA(f.zt[layer_D][-1, :, :]), label='Transformed Data')
        #         ax1.plot(ps.runPCA(f.zt[layer_D][0, :, :]), label='Original Data')
        #     else:
        #         ax1.plot([0], ps.runPCA(f.zt[layer_D][-1, :, :]), 'bo', label='Transformed Data')
        #         ax1.plot([0], ps.runPCA(f.zt[layer_D][0, :, :]), 'ro', label='Original Data')
        #     ax1.set_ylim(bottom=0)
        #     ax1.legend()
        #     ax1.set_xticks(range(f.zt[layer_D].shape[-1]))
        #     if f.zt[layer_D].shape[-1] > 1:
        #         ax2.plot(ps.runPCA(f.zt[layer_D][-1, :, :] - f.zt[layer_D][0, :, :]), label='Displacement')
        #     else:
        #         ax2.plot([0], ps.runPCA(f.zt[layer_D][-1, :, :] - f.zt[layer_D][0, :, :]), 'bo', label='Displacement')
        #     ax2.set_ylim(bottom=0)
        #     ax2.legend()
        #     ax2.set_xticks(range(f.zt[layer_D].shape[-1]))
        #     fig.suptitle(typeData + '_D' + str(layer_D))
        #     fig.canvas.draw_idle()
        #     fig.canvas.flush_events()
        #     fig.savefig(f.outputDir + '/' + typeData + '_D' + str(layer_D) + '.png')

        pykeops_cleanup = False


    loggingUtils.setup_default_logging(f.outputDir, fileName=loggingFile, stdOutput=True, mode='a')  # 'a' = write without overwriting
    logging.info('-' * 121)

    # analyze results
    logging.info('\n{0}  case {1}'.format(typeData0,case))
    logging.info('avg RMSE of train errors:   {0: .8f}  +/- {1: .8f}'.format(np.mean(np.sqrt(train_errors)),
                                                                             np.std(np.sqrt(train_errors)) / np.sqrt(nRuns)))
    logging.info('avg RMSE of test errors:    {0: .8f}  +/- {1: .8f}'.format(np.mean(np.sqrt(test_errors)),
                                                                             np.std(np.sqrt(test_errors)) / np.sqrt(nRuns)))
    logging.info('total run time:              {:0>8}'.format(str(timedelta(seconds=np.sum(times)))))
    logging.info('avg run time:                {:0>8}\n'.format(str(timedelta(seconds=np.mean(times)))))

    # close logging file
    logger = logging.getLogger()
    for h in logger.handlers:
        logger.removeHandler(h)
