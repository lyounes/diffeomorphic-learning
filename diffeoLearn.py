import os
from sys import platform
from operator import add
import logging
import copy
import time
import numpy as np
from sklearn.cluster import kmeans_plusplus
import matplotlib.pyplot as plt
import matplotlib
if 'DISPLAY' in os.environ or platform == 'darwin':
    matplotlib.use('qt5Agg')
    noDisplay = False
else:
    matplotlib.use("Agg")
    noDisplay = True
import pykeops
from base import loggingUtils
from base import pointSets as ps
from base import pointEvolution as evol, plotData
from base import conjugateGradient as cg, bfgs
from base import kernelFunctions as kfun, preProcesses as prep
from base.affineBasis import AffineBasis


class GaussDiff:
    def eval(self, u):
        return u*np.exp(- (u**2)/2)
    def evalDiff(self, u):
        uu = u*u
        return (1-uu)*np.exp(- (uu)/2)

class LinGaussDiff:
    def eval(self, u):
        return u + u*np.exp(- (u**2)/2)
    def evalDiff(self, u):
        uu = u*u
        return 1 + (1-uu)*np.exp(- (uu)/2)      # 1-u__*u__)*np.exp(- (u__ ** 2) / 2)

class Tanh:
    def eval(self, u):
        return np.tanh(u)
    def evalDiff(self, u):
        u = np.tanh(u)
        return 1 - u*u


class Direction:
    def __init__(self, vf_type='LDDMM', nD=1, nA=1):
        if vf_type == 'LDDMM':
            self.diff = [None] * nD
            self.aff = [None] * nD
            for layer_D in range(nD):
                self.diff[layer_D] = []
                self.aff[layer_D] = []
        elif vf_type == 'NN':
            self.W = [None] * nD
            self.b = [None] * nD
            for layer_D in range(nD):
                self.W[layer_D] = []
                self.b[layer_D] = []
        self.upred = [None] * nA
        self.bpred = [None] * nA
        for layer_A in range(nA):
            self.upred[layer_A] = []
            self.bpred[layer_A] = []


def __default_options__():
    opt = dict()

    opt['approach'] = 0                 # 0 = standard FineMorphs
    opt['model'] = 'regression'         # 'regression', 'classification', or 'reduction'
    opt['vf_type'] = 'LDDMM'            # vector field type ('LDDMM' or 'NN')

    opt['XTr_initFactor'] = 0.01        # XTr added dimension elements drawn from N(0,XTr_initFactor**2)
    opt['YTr_initFactor'] = 0.0         # YTr added dimension elements drawn from N(0,YTr_initFactor**2)

    opt['layers'] = ['A', 'D', 'A']     # order of A and D layers
    opt['layerOutDim'] = [None]         # number of dimensions to add to X before first layer
    opt['addDim_X'] = 1                 # number of dimensions to add to X before first layer
    opt['addDimLayer_In'] = [[], []]    # add zeros at beginning of layer (do not include layer 0)
    opt['removeDimLayer_In'] = [[], []] # dimensions to remove at beginning of layer (do not include layer 0)
    opt['addDim_predictedY'] = 0        # = (dimension of outputted predicted Y) - (dimension of Y) (only for 'regression')
    opt['addDim_Y'] = 0                 # number of dimensions to add to Y (only applicable for reduction)

    opt['freezeLayer'] = []

    opt['ball_eps'] = 0.5
    opt['rate'] = None

    opt['clamp'] = []
    opt['C'] = 1.0

    opt['patchesLayer'] = []            # break up these layers into actual patches (at beginning of layer)

    opt['repulseLayer_In'] = [[], []]
    opt['repulseLayer_Out'] = [[], []]
    opt['repulse_sigmaErrorFactor'] = 1.
    opt['zeroFlattenLayer_In'] = [[], []]
    opt['zeroFlattenLayer_Out'] = [[], []]
    opt['zeroFlatten_sigmaErrorFactor']: 1.
    opt['flattenLayer_In'] = [[], []]
    opt['flattenLayer_Out'] = [[], []]
    opt['flatten_sigmaErrorFactor'] = 1.
    opt['centerLayer_In'] = [[], []]
    opt['centerLayer_Out'] = [[], []]
    opt['center_sigmaErrorFactor'] = 1.
    opt['stitchLayer_In'] = [[], []]
    opt['stitchLayer_Out'] = [[], []]
    opt['stitch_sigmaErrorFactor'] = 1.
    opt['elasticPatchesLayer_In'] = [[], []]
    opt['elasticPatchesLayer_Out'] = [[], []]
    opt['elasticPatches_sigmaErrorFactor'] = 1.

    opt['ball_eps_elastic'] = 0.5
    opt['rate_elastic'] = None
    opt['elasticLayer_In'] = [[], []]
    opt['elasticLayer_Out'] = [[], []]
    opt['elastic_sigmaErrorFactor'] = 1.

    opt['lam'] = 1.                     # regularization weight for each A layer
    opt['ridgeType'] = 'reg'            # ridge type for each A layer:  'reg', 'scalar', 'diag', 'diag_eye', 'eye'
    opt['b_flag'] = True                # True = affine transformation includes an intercept/translation
    opt['u_initFactor'] = 0.01          # initial elements of u[1:,:] ~ N(0,u_initFactor**2)   (if Xavier_flag = False)
    opt['Xavier_flag'] = True           # True = apply Xavier initialization to u:  u_initFactor = sqrt(1./d1))

    opt['Tsize'] = 10                   # number of time steps for each D layer

    opt['trainSubset'] = [[],[],[]]     # [[layer_D],[method to chose the control points],[number of control points]]
                                        # method = 'KMeans', 'random', or 'patches'

    opt['gam'] = 1.                     # regularization weight for each D layer    (for vf_type == 'LDDMM')
    opt['at_initFactor'] = 0.0          # initial elements of at ~ N(0,at_initFactor**2)    (for vf_type == 'LDDMM')

    opt['affine'] = 'none'              # 'affine', 'similitude', 'euclidean', 'translation', 'diagonal', or 'none' (if 'LDDMM')
    opt['affineWeight'] = 10.           # multiplicative constant on affine regularization                          (if 'LDDMM')
    opt['rotWeight'] = 0.1              # multiplicative constant on rot regularization (supercedes affineWeight)   (if 'LDDMM')
    opt['scaleWeight'] = 10.            # multiplicative constant on scale regularization (supercedes affineWeight) (if 'LDDMM')
    opt['transWeight'] = 1.             # multiplicative constant on translation regularization (supercedes affineWeight) (if 'LDDMM')

    opt['name'] = 'lap'                 # kernel type ('gauss', 'lap', or 'min')
    opt['pykeops_flag'] = True          # True = use pykeops, False = use numba
    opt['dtype'] = 'float64'            # dtype for pykeops calculations
    opt['localMaps'] = None             #
    opt['affineK'] = None               # kernel affine component (None, 'affine', or 'euclidean')
    opt['w1'] = 1.0                     # weight for linear part (for affineK = 'affine' or 'euclidean')
    opt['w2'] = 1.0                     # weight for translation part (for affineK = 'affine' or 'euclidean')
    opt['center'] = None                # origin (for affineK = 'affine' or 'euclidean')
    opt['dim'] = 3                      # dimension (for affineK = 'euclidean')
    opt['order'] = 3                    # order for Laplacian kernel
    opt['kernelSigma'] = 0.5            # kernel widths for D layers

    opt['weights'] = [1., 1.]           # regularization weight for each D layer    (for vf_type == 'NN')
    opt['fun'] = LinGaussDiff           # GaussDiff, LinGaussDiff, or Tanh          (for vf_type == 'NN')
    opt['Wt_initFactor'] = 0.0          # initial elements of Wt ~ N(0,Wt_initFactor**2)    (for vf_type == 'NN')

    opt['alg'] = 'bfgs'                 # optimization algorithm ('bfgs or 'cg')
    opt['maxIter'] = 2000               # max iterations in bfgs or conjugate gradient
    opt['burnIn'] = 20
    opt['affBurnIn'] = 100
    opt['coeffuPred'] = 1.              # value for all A layers
    opt['coeffAff'] = 1.                # value for all D layers    (for vf_type == 'LDDMM')
    opt['gradCoeff'] = 1.               # normalizing coefficient for gradient
    opt['gradEps'] = -1                 # stopping threshold for small gradient; if = -1, calculated in optimizeMatching
    opt['objEps'] = 1e-8                # stopping threshold for small variation in obj
    opt['epsInit'] = 0.1                # initial gradient step
    opt['memory'] = 25                  # for bfgs
    opt['Wolfe'] = True                 # for bfgs or cg
    opt['verb'] = True                  # for verbose printing

    opt['sigmaErrorFactor'] = 1.        # factor for sigmaError, the normalization for the error term in cost function
    opt['update_sigmaError'] = True     # True = update sigmaError iteratively based on training error

    opt['testGradient'] = False         # True = evaluates gradient accuracy over random direction (debug)

    opt['saveRate'] = 20                # rate for plotting, saving files, and calculating errors
    opt['logging_flag'] = False         # create log file
    opt['loggingFile'] = 'logging_info' # filename for log file
    opt['saveTraj_flag'] = False        # save .vtk files and trajectory plots
    opt['save_layer_D'] = 0             # data positions:  index of D layer for saved trajectory plots or .vtk files (if =-1, uses original x data)
    opt['saveFile'] = 'evolution'       # baseline filename for .vtk files
    opt['plot_flag'] = False            # True = plot state in and state out of user-specified layers at saveRate
    opt['plot_layer'] = []              # data positions:  plot state in and state out of these layers
    opt['plot_ind'] = 0                 # data color:  index of y data for multivariate regression plots or .vtk files (automatically set to 0 for classif)
    opt['saveFigure_flag'] = False      # save figures
    opt['vmin_vmax_flag'] = False       # True = pin color vmin and vmax of all plots on subplot1 data (Original y dataset)

    return opt


class DiffeoLearn(object):

    ####################################################################################################################
    #   initialization start
    ####################################################################################################################

    def __init__(self, options=None):
        opt = __default_options__()
        if options is not None:
            for k in options.keys():
                opt[k] = options[k]
        self.__init_from_dict_(opt)


    def __init_from_dict_(self, opt):
        for k in opt.keys():
            setattr(self, k, opt[k])

        if noDisplay:
            self.plot_flag = False

        self.outputDir = os.getcwd() + '/output'
        if not os.path.exists(self.outputDir): os.makedirs(self.outputDir)

        # open logging file --------------------------------------------------------------------------------------------
        if self.logging_flag:
            loggingUtils.setup_default_logging(self.outputDir, fileName=self.loggingFile, stdOutput=True, mode='w')  # 'w' = overwrite file (if exists)
        else:
            loggingUtils.setup_default_logging(stdOutput=False)

        self.numLayers = len(self.layers)
        self.nA = self.layers.count('A')  # number of A layers
        self.nD = self.layers.count('D')  # number of D layers

        if self.nA == 0:
            self.lam = [None] * self.nA
            self.ridgeType = [None] * self.nA
            self.b_flag = [None] * self.nA
            self.u_initFactor = [None] * self.nA
            self.Xavier_flag = [None] * self.nA

        self.freezeLayer = [True if layer in self.freezeLayer else False for layer in range(self.numLayers)]

        if not isinstance(self.lam, list): self.lam = [self.lam] * self.nA
        if not isinstance(self.ridgeType, list): self.ridgeType = [self.ridgeType] * self.nA
        if not isinstance(self.b_flag, list): self.b_flag = [self.b_flag] * self.nA
        if not isinstance(self.u_initFactor, list): self.u_initFactor = [self.u_initFactor] * self.nA
        if not isinstance(self.Xavier_flag, list): self.Xavier_flag = [self.Xavier_flag] * self.nA

        if not isinstance(self.Tsize, list): self.Tsize = [self.Tsize] * self.nD

        if not isinstance(self.gam, list): self.gam = [self.gam] * self.nD
        if not isinstance(self.at_initFactor, list): self.at_initFactor = [self.at_initFactor] * self.nD

        if not isinstance(self.affine, list): self.affine = [self.affine] * self.nD
        if not isinstance(self.affineWeight, list): self.affineWeight = [self.affineWeight] * self.nD
        if not isinstance(self.rotWeight, list): self.rotWeight = [self.rotWeight] * self.nD
        if not isinstance(self.scaleWeight, list): self.scaleWeight = [self.scaleWeight] * self.nD
        if not isinstance(self.transWeight, list): self.transWeight = [self.transWeight] * self.nD

        if not isinstance(self.name, list): self.name = [self.name] * self.nD
        if not isinstance(self.pykeops_flag, list): self.pykeops_flag = [self.pykeops_flag] * self.nD
        if not isinstance(self.dtype, list): self.dtype = [self.dtype] * self.nD
        if not isinstance(self.localMaps, list): self.localMaps = [self.localMaps] * self.nD
        if not isinstance(self.affineK, list): self.affineK = [self.affineK] * self.nD
        if not isinstance(self.w1, list): self.w1 = [self.w1] * self.nD
        if not isinstance(self.w2, list): self.w2 = [self.w2] * self.nD
        if not isinstance(self.center, list): self.center = [self.center] * self.nD
        if not isinstance(self.dim, list): self.dim = [self.dim] * self.nD
        if not isinstance(self.order, list): self.order = [self.order] * self.nD
        if not isinstance(self.kernelSigma, list): self.kernelSigma = [self.kernelSigma] * self.nD

        if not isinstance(self.weights[0], list): self.weights = [self.weights] * self.nD
        if not isinstance(self.fun, list): self.fun = [self.fun] * self.nD
        if not isinstance(self.Wt_initFactor, list): self.Wt_initFactor = [self.Wt_initFactor] * self.nD

        self.timeStep = [1. / item for item in self.Tsize]

        logging.info('-' * 121)
        logging.info('vf_type = {0}'.format(self.vf_type))

        # create kernels for each D layer ------------------------------------------------------------------------------
        if self.vf_type == 'LDDMM':
            if len(self.kernelSigma) != self.nD:
                logging.info('\nkernelSigma should have {0} entries'.format(self.nD))
                exit()
            logging.info('kernel sigmas = {}'.format(self.kernelSigma))
            self.KparDiff = [None] * self.nD
            for layer_D in range(self.nD):
                self.KparDiff[layer_D] = kfun.Kernel(name=self.name[layer_D], affine=self.affineK[layer_D],
                                                     sigma=self.kernelSigma[layer_D], order=self.order[layer_D],
                                                     w1=self.w1[layer_D], w2=self.w2[layer_D],
                                                     center=self.center[layer_D],
                                                     dim=self.dim[layer_D], localMaps=self.localMaps[layer_D],
                                                     dtype=self.dtype[layer_D], pykeops_flag=self.pykeops_flag[layer_D])
            if any(self.pykeops_flag):
                logging.info('pykeops.config.gpu_available = {0}'.format(pykeops.config.gpu_available))

        # check plot indices -------------------------------------------------------------------------------------------
        if self.plot_flag or self.saveTraj_flag:
            if self.model == 'classification':
                self.plot_ind = 0
                logging.info('\nself.plot_ind set to 0')
            if self.saveTraj_flag and (self.save_layer_D > self.nD - 1):
                logging.info('\nself.save_layer_D must be < self.nD')
                exit()

        # set some flags -----------------------------------------------------------------------------------------------
        self.repulse_flag = self.repulseLayer_In[0] or self.repulseLayer_Out[0]
        self.elastic_flag = self.elasticLayer_In[0] or self.elasticLayer_Out[0]
        self.elasticPatches_flag = self.elasticPatchesLayer_In[0] or self.elasticPatchesLayer_Out[0]
        self.zeroFlatten_flag = self.zeroFlattenLayer_In[0] or self.zeroFlattenLayer_Out[0]
        self.flatten_flag = self.flattenLayer_In[0] or self.flattenLayer_Out[0]
        self.center_flag = self.centerLayer_In[0] or self.centerLayer_Out[0]
        self.calc_patches_flag = self.patchesLayer or self.repulse_flag or self.flatten_flag or self.center_flag \
                                 or self.elasticPatches_flag or ('patches' in self.trainSubset[1]) or (self.approach in (1,2))
        if any([True if self.layers[layer]=='A' else False for layer in self.patchesLayer]):
            logging.info('\ncannot create patches in an A layer')
            exit()
        self.createPatches = [False] * self.numLayers
        if self.patchesLayer: self.createPatches[min(self.patchesLayer)] = True
        self.stitchPatches_flag = self.patchesLayer and (self.stitchLayer_In[0] or self.stitchLayer_Out[0])
        if self.stitchPatches_flag:
            if self.stitchLayer_In[0]:
                if min(self.patchesLayer) >= self.stitchLayer_In[0][0]:
                    logging.info('\nstitching occuring before patches created')
                    exit()
            if self.stitchLayer_Out[0]:
                if min(self.patchesLayer) > self.stitchLayer_Out[0][0]:
                    logging.info('\nstitching occuring before patches created')
                    exit()
        if self.elastic_flag and self.patchesLayer:
            logging.info('\ncannot use elastic constraint and create patches')
            exit()
        if self.elasticPatches_flag and not any(self.createPatches):
            logging.info('\ncannot use elasticPatches constraint without patches')
            exit()

        # close logging file -------------------------------------------------------------------------------------------
        logger = logging.getLogger()
        for h in logger.handlers:
            logger.removeHandler(h)


    def __init_plots__(self):
        if self.plot_flag or self.saveTraj_flag:
            if self.plot_flag:
                self.figTrain = plt.figure('Training Data', figsize=(19, 8)) # (14, 6)
                self.figTrain.clf()
                mngr = plt.get_current_fig_manager()
                geom = mngr.window.geometry()
                x, y, dx, dy = geom.getRect()
                mngr.window.setGeometry(5, 30, dx, dy)
            if self.saveTraj_flag:
                ps.savePoints(self.outputDir + '/Template_Train.vtk', self.XTr)
            self.plots_are_initialized = True


    def first_plot(self):
        if self.plot_flag or self.saveTraj_flag:
            if not self.plots_are_initialized:
                self.__init_plots__()
            if self.cTr is None:
                self.colors = np.copy(self.YTr[:, self.plot_ind])
                self.Y = self.YTr[:, [self.plot_ind]]
                self.colors_layer = [np.copy(self.YTr_layer[layer][:, self.plot_ind]) for layer in range(self.numLayers)]
                self.Y_layer = [np.copy(self.YTr_layer[layer][:, [self.plot_ind]]) for layer in range(self.numLayers)]
            else:
                self.colors = np.copy(self.cTr)
                self.Y = None
                self.colors_layer = [np.copy(self.cTr_layer[layer]) for layer in range(self.numLayers)]
                self.Y_layer = [None] * self.numLayers
            self.x3_orig_train = ps.build3DProjection(self.XTr[:, :self.dim_X], Y=self.Y, mode=self.model)
            if self.saveTraj_flag:
                ps.savePoints(self.outputDir + '/TrainingSet.vtk', self.x3_orig_train, scalars=self.colors)
            if self.plot_flag:
                plotData.plotArray([self.x3_orig_train] * (2*len(self.plot_layer) + 1),
                                    [self.colors] * (2*len(self.plot_layer) + 1),
                                    fig=self.figTrain,
                                    titles=['Original'] * (2*len(self.plot_layer) + 1),
                                    suptitle=''.join(self.layers) + ' Sequence',
                                    vmin_vmax_flag=self.vmin_vmax_flag, vmin_vmax=self.vmin_vmax)
                plt.pause(0.1)


    def running_plot(self):
        if self.plot_flag or self.saveTraj_flag:
            if not self.plots_are_initialized:
                self.__init_plots__()
            if self.saveTraj_flag:
                if self.save_layer_D == -1:
                    savelayerD = 0
                else:
                    savelayerD = np.copy(self.save_layer_D)
                x3Tr_ = np.zeros((self.zt[savelayerD].shape[0], self.zt[savelayerD].shape[1], 3))
                for kk in range(self.Tsize[savelayerD] + 1):
                    x3Tr_[kk,:,:] = ps.build3DProjection(self.zt[savelayerD][kk, :, :], Y=self.Y_layer[self.layer_D_indices[savelayerD]], mode=self.model)
                    ps.savePoints(self.outputDir + '/' + self.saveFile + str(kk) + '.vtk',
                                  x3Tr_[kk,:,:], scalars=self.colors_layer[self.layer_D_indices[savelayerD]])
                fig_ = plt.figure()
                fig_.clf()
                ax_ = fig_.add_subplot(111, projection='3d')
                delta = 1e-12
                axes_ = [x3Tr_[:, :, 0].min()-delta, x3Tr_[:, :, 0].max()+delta,
                         x3Tr_[:, :, 1].min()-delta, x3Tr_[:, :, 1].max()+delta,
                         x3Tr_[:, :, 2].min()-delta, x3Tr_[:, :, 2].max()+delta]
                for kk in range(self.Tsize[savelayerD] + 1):
                    ax_.cla()
                    ax_.set_xlim3d(axes_[0], axes_[1])
                    ax_.set_ylim3d(axes_[2], axes_[3])
                    ax_.set_zlim3d(axes_[4], axes_[5])
                    cmap_ = plt.cm.jet
                    vmin_ = np.amin(np.ravel(self.colors_layer[self.layer_D_indices[savelayerD]]))
                    vmax_ = np.amax(np.ravel(self.colors_layer[self.layer_D_indices[savelayerD]]))
                    norm_ = matplotlib.colors.Normalize(vmin_, vmax_)
                    sm_ = plt.cm.ScalarMappable(cmap=cmap_, norm=norm_)
                    sm_.set_array([])
                    c_ = np.ravel(self.colors_layer[self.layer_D_indices[savelayerD]])
                    ax_.scatter(x3Tr_[kk, :, 0], x3Tr_[kk, :, 1], x3Tr_[kk, :, 2], marker='o', s=2, c=cmap_(norm_(c_)))
                    plt.savefig(self.outputDir + '/train' + str(kk) + '.png')
                plt.close(fig_)
            if self.plot_flag:
                x3Tr_In = [ps.build3DProjection(self.forwardState_In[entry], mode=self.model)
                            for entry in self.plot_layer]
                x3Tr_Out = [ps.build3DProjection(self.forwardState_Out[entry], mode=self.model)
                            for entry in self.plot_layer]
                x3Tr_In_Out = [item for pair in zip(x3Tr_In, x3Tr_Out + [0]) for item in pair]
                colors_In = [self.colors_layer[entry] for entry in self.plot_layer]
                colors_In_Out = [val for val in colors_In for _ in range(2)]
                State_In_str = ['State In of Layer ' + str(entry) for entry in self.plot_layer]
                State_Out_str = ['State Out of Layer ' + str(entry) for entry in self.plot_layer]
                State_str = [item for pair in zip(State_In_str, State_Out_str + [0]) for item in pair]
                plotData.plotArray([self.x3_orig_train] + x3Tr_In_Out,
                                   [self.colors] + colors_In_Out,
                                   fig=self.figTrain,
                                   titles=['Original'] + State_str,
                                   suptitle=''.join(self.layers) + ' Sequence',
                                   vmin_vmax_flag=self.vmin_vmax_flag, vmin_vmax=self.vmin_vmax)
                if self.saveFigure_flag:
                    plt.savefig(self.outputDir + '/train.png')
                time.sleep(0.5)

    ####################################################################################################################
    #   initialization stop
    ####################################################################################################################

    ####################################################################################################################
    #   diffeoLearn start
    ####################################################################################################################

    def fit(self, fit_x, fit_y, cTr=None, typeData='data', standardize_fit_x=True, standardize_fit_y=True, set_random_seed=False,
            pykeops_cleanup=False      # pykeops clean-up, in case old build files are still present
            ):

        if self.vf_type == 'LDDMM':
            if self.pykeops_flag and pykeops_cleanup:
                pykeops.clean_pykeops()

        self.cTr = cTr

        if self.logging_flag:
            loggingUtils.setup_default_logging(self.outputDir, fileName=self.loggingFile, stdOutput=True, mode='a')  # 'a' = write without overwriting
        else:
            loggingUtils.setup_default_logging(stdOutput=False)

        logging.info('-' * 121)
        logging.info('\n{0:s}\n'.format(typeData))

        if set_random_seed:
            print('setting random seed')
            np.random.seed(42)

        # standardize x and y data -------------------------------------------------------------------------------------
        self.standardize_fit_x = standardize_fit_x
        self.standardize_fit_y = standardize_fit_y
        self.XTr = np.copy(fit_x)
        self.YTr = np.copy(fit_y)
        self.dim_X = self.XTr.shape[1]
        self.NTr = self.XTr.shape[0]
        self.mu0_x = np.zeros((1,self.XTr.shape[1]))
        self.s0_x = np.ones((1,self.XTr.shape[1]))
        self.mu0_y = np.zeros((1,self.YTr.shape[1]))
        self.s0_y = np.ones((1,self.YTr.shape[1]))
        for dimY in range(self.addDim_Y):
            self.s0_y = np.concatenate((self.s0_y, np.array([[1.0]])), axis=1)
            self.mu0_y = np.concatenate((self.mu0_y, np.array([[0.0]])), axis=1)
        if self.standardize_fit_x:
            self.XTr, self.mu0_x, self.s0_x = prep.standardizeData(self.XTr)
        if self.model == 'regression':
            self.dim_Y = self.YTr.shape[1]
            if self.standardize_fit_y:
                self.YTr, self.mu0_y, self.s0_y = prep.standardizeData(self.YTr, standardize='y')
        elif self.model == 'classification':
            self.nClasses, self.wTr, self.swTr = prep.standardizeData(self.YTr, standardize='y', model='classification')  # calculate nClasses, wTr, and swTr
            self.dim_Y = int(np.copy(self.nClasses))
            self.s0_y = None
            self.addDim_predictedY = 0
            self.addDim_Y = 0
        logging.info('XTr.shape = {0}'.format(self.XTr.shape))
        logging.info('YTr.shape = {0}'.format(self.YTr.shape))
        self.NTr_layer = [self.NTr] * self.numLayers
        if self.cTr is not None:
            self.cTr_layer = [self.cTr] * self.numLayers
        self.vmin_vmax = None


        # assign dimensions to D layers and A layers -------------------------------------------------------------------
        self.layerOutDim, self.dim_D, self.dim_A, self.dim_all_layers, self.layer_D_indices, self.layer_A_indices, self.ridgeType = \
            prep.assignDimensions(self.layers, self.layerOutDim, self.dim_X, self.dim_Y, self.nD, self.nA,
                                  self.addDim_X, self.addDimLayer_In, self.removeDimLayer_In, self.addDim_predictedY,
                                  self.addDim_Y, self.ridgeType)


        # modify Wt_initFactor -----------------------------------------------------------------------------------------
        if self.vf_type == 'NN':
            for layer_D in range(self.nD):
                self.Wt_initFactor[layer_D] = np.sqrt(1. / self.dim_D[layer_D])


        # calc patches, if applicable ----------------------------------------------------------------------------------
        self.nPatches = 1
        if self.calc_patches_flag:
            self.points_in_patch, self.patch_centers, self.overlapping_patches, self.non_overlapping_patches, self.overlapping_points, self.ball_eps \
                = prep.calcPatches(self.XTr, labels=self.cTr, ball_eps=self.ball_eps, rate=self.rate, small_dataset=True, relabel_flag=True,
                                   consolidate_patches=True, no_overlap=True)
            self.nPatches = len(self.points_in_patch)
            logging.info(f'nPatches = {self.nPatches:d}')
            self.centerIndices = [self.patch_centers[0]] * self.numLayers
            # if patches will also be created:
            if self.patchesLayer:
                self.NTr_patch = [self.points_in_patch[patch].size for patch in range(self.nPatches)]
                logging.info('NTr_patch = {0}'.format(self.NTr_patch))
                NTr_all_patches = sum(self.NTr_patch)
                self.idx_patches = [np.arange(int(sum(self.NTr_patch[:patch])), int(sum(self.NTr_patch[:patch + 1]))) for patch in range(self.nPatches)]
                if self.cTr is not None:
                    cTr_all_patches = np.copy(self.cTr[self.points_in_patch[0]])[:, np.newaxis]
                    for patch in range(1, self.nPatches):
                        cTr_all_patches = np.concatenate((cTr_all_patches, self.cTr[self.points_in_patch[patch]][:, np.newaxis]), axis=0)
                    cTr_all_patches = cTr_all_patches[:,0]


        # if creating patches, modify some parameters ------------------------------------------------------------------
        if any(self.createPatches):
            for layer in range(self.createPatches.index(True), self.numLayers):
                self.centerIndices[layer] = self.patch_centers[1]
                self.NTr_layer[layer] = NTr_all_patches
                if self.cTr is not None:
                    self.cTr_layer[layer] = cTr_all_patches
        logging.info('NTr_layer = {0}'.format(self.NTr_layer))


        # calc elastic neighborhoods, if applicable --------------------------------------------------------------------
        if self.elastic_flag: # no patches created
            self.points_in_patch_elastic, self.patch_centers_elastic, self.overlapping_patches_elastic, \
            self.non_overlapping_patches_elastic, self.overlapping_points_elastic, self.ball_eps_elastic \
                = prep.calcPatches(self.XTr, labels=self.cTr, ball_eps=self.ball_eps_elastic, rate=self.rate_elastic, small_dataset=True,
                                   patch_on_every_point=True)
            self.nPatches_elastic = len(self.points_in_patch_elastic)
            logging.info(f'nPatches_elastic = {self.nPatches_elastic:d}')

            self.NTr_patch_elastic = [len(self.overlapping_patches_elastic[patch]) for patch in range(self.nPatches_elastic)]
            # logging.info('NTr_patch_elastic = {0}'.format(self.NTr_patch_elastic))
            NTr_all_patches_elastic = sum(self.NTr_patch_elastic)
            # logging.info('NTr_all_patches_elastic = {0}'.format(NTr_all_patches_elastic))
            first_indices = np.zeros(NTr_all_patches_elastic, dtype=int)
            second_indices = np.zeros(NTr_all_patches_elastic, dtype=int)
            idx = 0
            for j in range(self.nPatches_elastic):
                for j_prime in self.overlapping_patches_elastic[j]:
                    first_indices[idx] = int(j)
                    second_indices[idx] = int(j_prime)
                    idx += 1
            self.centerIndices_elastic = [[first_indices.astype(int), second_indices.astype(int)] for layer in range(self.numLayers)]
            self.XTr_centerIndices_elastic = [first_indices.astype(int), second_indices.astype(int)]
        if self.elasticPatches_flag: # patches created
            first_indices = np.zeros(NTr_all_patches-self.nPatches, dtype=int)
            second_indices = np.zeros(NTr_all_patches-self.nPatches, dtype=int)
            XTr_first_indices = np.zeros(NTr_all_patches - self.nPatches, dtype=int)
            XTr_second_indices = np.zeros(NTr_all_patches - self.nPatches, dtype=int)
            idx = 0
            for j in range(self.nPatches):
                idx_prime = 0
                for j_prime in self.idx_patches[j]:
                    if j_prime != self.patch_centers[1][j]:
                        first_indices[idx] = int(self.patch_centers[1][j])
                        second_indices[idx] = int(j_prime)
                        XTr_first_indices[idx] = int(self.patch_centers[0][j])
                        XTr_second_indices[idx] = int(self.points_in_patch[j][idx_prime])
                        idx += 1
                    idx_prime += 1
            self.centerIndices_elasticPatches = [[first_indices.astype(int), second_indices.astype(int)] for layer in range(self.numLayers)]
            self.XTr_centerIndices_elasticPatches = [XTr_first_indices.astype(int), XTr_second_indices.astype(int)]


        # estimate training threshold ----------------------------------------------------------------------------------
        _s = prep.estimate_training_threshold(self.XTr, self.YTr, self.s0_y, prediction=self.model)


        # initialize sigmaError ----------------------------------------------------------------------------------------
        # sigmaError: normalization for the error term
        if self.model == 'regression':
            self.sigmaError = ((self.NTr_layer[-1]) ** .25) * max(np.sqrt((_s / self.s0_y ** 2).sum()), 0.1)
        elif self.model == 'classification':
            self.sigmaError = ((self.NTr_layer[-1]) ** .5) * 0.01
        self.sigmaError *= self.sigmaErrorFactor
        self.sigmaError = self.sigmaErrorFactor # temporary - just for autoencoder
        logging.info(f'sigmaError = {self.sigmaError:.4f}')


        # if forces or constraints applied at any layer, calculate corresponding sigmaErrors ---------------------------
        if self.repulse_flag:
            self.repulse_sigmaError_non_overlap = [[0.0 for j_prime in range(self.nPatches)] for j in range(self.nPatches)]
            for j in range(self.nPatches):
                for j_prime in self.non_overlapping_patches[j]:  # j_prime > j in non_overlapping_patches[j]
                    self.repulse_sigmaError_non_overlap[j][j_prime] = self.repulse_sigmaErrorFactor
            self.repulse_sigmaError_overlap = [[0.0 for j_prime in range(self.nPatches)] for j in range(self.nPatches)]
            for j in range(self.nPatches):
                for j_prime in self.overlapping_patches[j]:  # j_prime > j in overlapping_patches[j]
                    self.repulse_sigmaError_overlap[j][j_prime] = self.repulse_sigmaErrorFactor
            logging.info(f'repulse_sigmaError = {self.repulse_sigmaErrorFactor:.4f}')
        if self.elastic_flag:
            # self.elastic_sigmaError = [[0.0 for j_prime in range(self.nPatches_elastic)] for j in range(self.nPatches_elastic)]
            # for j in range(self.nPatches_elastic):
                # for j_prime in self.overlapping_patches_elastic[j]:  # j_prime > j in overlapping_patches_elastic[j]
                #     self.elastic_sigmaError[j][j_prime] = self.elastic_sigmaErrorFactor
            self.elastic_sigmaError = self.elastic_sigmaErrorFactor
            logging.info(f'elastic_sigmaError = {self.elastic_sigmaErrorFactor:.4f}')
        if self.elasticPatches_flag:
            self.elasticPatches_sigmaError = self.elasticPatches_sigmaErrorFactor
            logging.info(f'elasticPlastic_sigmaError = {self.elasticPatches_sigmaErrorFactor:.4f}')
        if self.zeroFlatten_flag:
            self.zeroFlatten_sigmaError = self.zeroFlatten_sigmaErrorFactor
            logging.info(f'zeroFlatten_sigmaError = {self.zeroFlatten_sigmaErrorFactor:.4f}')
        if self.flatten_flag:
            self.flatten_sigmaError = [[0.0 for j_prime in range(self.nPatches)] for j in range(self.nPatches)]
            for j in range(self.nPatches):
                for j_prime in self.overlapping_patches[j]:  # j_prime > j in overlapping_patches[j]
                    self.flatten_sigmaError[j][j_prime] = self.flatten_sigmaErrorFactor
            logging.info(f'flatten_sigmaError = {self.flatten_sigmaErrorFactor:.4f}')
        if self.center_flag:
            self.center_sigmaError = [[0.0 for j_prime in range(self.nPatches)] for j in range(self.nPatches)]
            for j in range(self.nPatches):
                for j_prime in self.overlapping_patches[j]:  # j_prime > j in overlapping_patches[j]
                    self.center_sigmaError[j][j_prime] = self.center_sigmaErrorFactor
            logging.info(f'center_sigmaError = {self.center_sigmaErrorFactor:.4f}')
        if self.stitchPatches_flag:
            self.stitch_sigmaError = [[0.0 for j_prime in range(self.nPatches)] for j in range(self.nPatches)]
            for j in range(self.nPatches):
                for j_prime in self.overlapping_patches[j]:  # j_prime > j in overlapping_patches[j]
                    self.stitch_sigmaError[j][j_prime] = self.stitch_sigmaErrorFactor
            logging.info(f'stitch_sigmaError = {self.stitch_sigmaErrorFactor:.4f}')


        # assign indices of data set parametrizing optimal or sub-optimal vector fields --------------------------------
        self.trainSubset_flag = [False] * self.nD  # True = sub-optimal vector field training
        self.nControlPts = [[None for patch in range(self.nPatches)] if self.layer_D_indices[layer_D] in self.patchesLayer else None for layer_D in range(self.nD)]  # Number of control points
        self.idx_diff = [[None for patch in range(self.nPatches)] if self.layer_D_indices[layer_D] in self.patchesLayer else [None] for layer_D in range(self.nD)]
        self.idx_diff_shifted = [[None for patch in range(self.nPatches)] if self.layer_D_indices[layer_D] in self.patchesLayer else [None] for layer_D in range(self.nD)]
        self.idx_nodiff = [[None for patch in range(self.nPatches)] if self.layer_D_indices[layer_D] in self.patchesLayer else [None] for layer_D in range(self.nD)]
        self.idx_nodiff_shifted = [[None for patch in range(self.nPatches)] if self.layer_D_indices[layer_D] in self.patchesLayer else [None] for layer_D in range(self.nD)]
        self.idx_diff_nodiff = [[None for patch in range(self.nPatches)] if self.layer_D_indices[layer_D] in self.patchesLayer else [None] for layer_D in range(self.nD)]
        self.idx_controls = [[None for patch in range(self.nPatches)] if self.layer_D_indices[layer_D] in self.patchesLayer else [None] for layer_D in range(self.nD)]
        self.N_controls = [None] * self.nD
        self.idx_clamp = [None] * self.nD
        for layer_D in range(self.nD):
            if layer_D in self.trainSubset[0]:
                self.trainSubset_flag[layer_D] = True
                if self.layer_D_indices[layer_D] in self.patchesLayer:
                    logging.info('cannot use trainSubset and propagate patches in same D layer')
                    exit()
                else:
                    if self.trainSubset[1][self.trainSubset[0].index(layer_D)] == 'KMeans':
                        self.nControlPts[layer_D] = self.trainSubset[2][self.trainSubset[0].index(layer_D)]
                        centers, idx_diff_layer_D = kmeans_plusplus(self.XTr, n_clusters=self.nControlPts[layer_D])
                        self.idx_diff[layer_D] = [idx_diff_layer_D]
                        self.idx_nodiff[layer_D] = [np.setdiff1d(np.arange(self.NTr_layer[self.layer_D_indices[layer_D]]), self.idx_diff[layer_D][0])]
                    elif self.trainSubset[1][self.trainSubset[0].index(layer_D)] == 'random':
                        self.nControlPts[layer_D] = self.trainSubset[2][self.trainSubset[0].index(layer_D)]
                        idx = np.random.permutation(self.NTr_layer[self.layer_D_indices[layer_D]])
                        self.idx_diff[layer_D] = [idx[:self.nControlPts[layer_D]]]
                        self.idx_nodiff[layer_D] = [idx[self.nControlPts[layer_D]:]]
                    elif self.trainSubset[1][self.trainSubset[0].index(layer_D)] == 'patches':
                        self.nControlPts[layer_D] = self.nPatches
                        self.idx_diff[layer_D] = [self.centerIndices[self.layer_D_indices[layer_D]]]
                        self.idx_nodiff[layer_D] = [np.setdiff1d(np.arange(self.NTr_layer[self.layer_D_indices[layer_D]]), self.idx_diff[layer_D][0])]
                    self.idx_diff_nodiff[layer_D] = [np.arange(self.NTr_layer[self.layer_D_indices[layer_D]])]
                    self.idx_diff_shifted[layer_D] = self.idx_diff[layer_D]
                    self.idx_nodiff_shifted[layer_D] = self.idx_nodiff[layer_D]
                    self.idx_controls[layer_D] = [np.arange(self.nControlPts[layer_D])]
                    self.N_controls[layer_D] = self.nControlPts[layer_D]
            else:
                if self.layer_D_indices[layer_D] in self.patchesLayer:
                    self.nControlPts[layer_D] = [self.NTr_patch[patch] for patch in range(self.nPatches)]
                    self.idx_diff[layer_D] = self.idx_patches
                    self.idx_diff_shifted[layer_D] = [self.idx_diff[layer_D][patch] - sum(self.NTr_patch[:patch]) for patch in range(self.nPatches)]
                    self.idx_diff_nodiff[layer_D] = self.idx_patches
                    self.idx_controls[layer_D] = [np.arange(int(sum(self.nControlPts[layer_D][:patch])), int(sum(self.nControlPts[layer_D][:patch + 1])))
                                                 for patch in range(self.nPatches)]
                    self.N_controls[layer_D] = sum(self.nControlPts[layer_D])
                else:
                    self.nControlPts[layer_D] = self.NTr_layer[self.layer_D_indices[layer_D]]
                    self.idx_diff[layer_D] = [np.arange(self.NTr_layer[self.layer_D_indices[layer_D]])]
                    self.idx_diff_shifted[layer_D] = self.idx_diff[layer_D]
                    self.idx_diff_nodiff[layer_D] = [np.arange(self.NTr_layer[self.layer_D_indices[layer_D]])]
                    self.idx_controls[layer_D] = [np.arange(self.nControlPts[layer_D])]
                    self.N_controls[layer_D] = self.nControlPts[layer_D]
        logging.info('nControlPts = {0}'.format(self.nControlPts))
        logging.info('N_controls = {0}'.format(self.N_controls))


        # modify self.XTr and self.YTr (i.e., add dimension(s)), if applicable -----------------------------------------
        # if addDim_X>0, add dimensions to self.XTr
        if self.addDim_X > 0:
            if isinstance(self.idx_diff[0],list):
                addCol = self.XTr_initFactor * np.random.normal(size=(self.NTr, self.addDim_X))
            else:
                addCol = np.zeros((self.NTr, self.addDim_X))
                addCol[self.idx_diff[0], :] = self.XTr_initFactor * np.random.normal(size=(self.idx_diff[0], self.addDim_X))
            self.XTr = np.concatenate((self.XTr, addCol), axis=1)
            logging.info('updated XTr.shape = ({0:d},{1:d})'.format(self.XTr.shape[0], self.XTr.shape[1]))
        # if addDim_Y>0, add dimensions to self.YTr
        if self.addDim_Y > 0:
            addCol = self.YTr_initFactor * np.random.normal(size=(self.NTr, self.addDim_Y))
            self.YTr = np.concatenate((self.YTr, addCol), axis=1)
            for dimY in range(self.addDim_Y):
                self.s0_y = np.concatenate((self.s0_y, np.array([[1.0]])), axis=1)
                self.mu0_y = np.concatenate((self.mu0_y, np.array([[0.0]])), axis=1)
            self.dim_Y += self.addDim_Y
            logging.info('updated YTr.shape = ({0:d},{1:d})'.format(self.YTr.shape[0], self.YTr.shape[1]))
        self.YTr_layer = [self.YTr] * self.numLayers
        if self.patchesLayer:
            YTr_all_patches = np.copy(self.YTr[self.points_in_patch[0], :])
            for patch in range(1, self.nPatches):
                YTr_all_patches = np.concatenate((YTr_all_patches, self.YTr[self.points_in_patch[patch], :]), axis=0)
            priorPatches = False
            for layer in range(self.numLayers):
                if (layer in self.patchesLayer) or priorPatches:
                    priorPatches = True
                    self.YTr_layer[layer] = YTr_all_patches
            if self.model == 'classification':
                nTr = np.array([(YTr_all_patches == k).sum() for k in range(self.nClasses)])
                self.wTr = float(YTr_all_patches.size) / (nTr[YTr_all_patches[:, 0]] * self.nClasses)[:, np.newaxis]
                self.swTr = self.wTr.sum()


        # if approach == 1 or 2, re-initialize some terms --------------------------------------------------------------
        self.XTr_orig = np.copy(self.XTr)
        if self.approach == 1:
            print('C = ', self.C)
            self.nLoops = np.copy(self.nPatches)
            self.XTr_orig = np.copy(self.XTr)
            self.XTr_orig_transformed = np.copy(self.XTr)
            self.previous_nested_points = None
            self.previous_nested_points_local = None
            self.new_points = np.copy(self.points_in_patch[0])
            self.new_points_local = np.arange(self.new_points.size)
            self.nested_points = self.points_in_patch[0]
            self.XTr = np.copy(self.XTr_orig_transformed[self.nested_points, :])
            self.NTr_layer = [self.nested_points.size] * self.numLayers
            self.YTr_orig_transformed = np.copy(self.YTr)
            self.YTr_layer = [self.YTr[self.nested_points, :]] * self.numLayers
            if self.cTr is not None:
                self.cTr_orig = np.copy(self.cTr)
                self.cTr = self.cTr_orig[self.nested_points]
                self.cTr_layer = [self.cTr] * self.numLayers
                self.vmin_vmax = [np.amin(np.ravel(self.cTr_orig)), np.amax(np.ravel(self.cTr_orig))]
                self.colors_layer = [np.copy(self.cTr_layer[layer]) for layer in range(self.numLayers)]
            self.nControlPts = [self.NTr_layer[self.layer_D_indices[layer_D]] for layer_D in range(self.nD)]
            self.idx_diff = [[np.arange(self.NTr_layer[self.layer_D_indices[layer_D]])] for layer_D in range(self.nD)]
            self.idx_diff_shifted = self.idx_diff
            self.idx_diff_nodiff = self.idx_diff
            self.idx_controls = [[np.arange(self.nControlPts[layer_D])] for layer_D in range(self.nD)]
            self.N_controls = [self.nControlPts[layer_D] for layer_D in range(self.nD)]
            logging.info('\nPatch ' + str(0) + '\n')
            logging.info('nControlPts = {0}'.format(self.nControlPts))
            logging.info('N_controls = {0}'.format(self.N_controls))
            self.nPatches = 1
        elif self.approach == 2:
            self.nD_encoder = self.nPatches
            self.nD_decoder = self.nPatches # 1
            self.layers = ['D'] * self.nD_encoder + ['D'] * self.nD_decoder
            self.numLayers = len(self.layers)
            self.nD = self.layers.count('D')

            self.dim_D = [self.dim_D[0]] * self.nD_encoder + [self.dim_D[1]] * self.nD_decoder
            self.Tsize = [self.Tsize[0]] * self.nD_encoder + [self.Tsize[1]] * self.nD_decoder
            self.gam = [self.gam[0]] * self.nD_encoder + [self.gam[1]] * self.nD_decoder
            self.at_initFactor = [self.at_initFactor[0]] * self.nD_encoder + [self.at_initFactor[1]] * self.nD_decoder
            self.affine = [self.affine[0]] * self.nD_encoder + [self.affine[1]] * self.nD_decoder
            self.affineWeight = [self.affineWeight[0]] * self.nD_encoder + [self.affineWeight[1]] * self.nD_decoder
            self.rotWeight = [self.rotWeight[0]] * self.nD_encoder + [self.rotWeight[1]] * self.nD_decoder
            self.scaleWeight = [self.scaleWeight[0]] * self.nD_encoder + [self.scaleWeight[1]] * self.nD_decoder
            self.transWeight = [self.transWeight[0]] * self.nD_encoder + [self.transWeight[1]] * self.nD_decoder
            self.timeStep = [self.timeStep[0]] * self.nD_encoder + [self.timeStep[1]] * self.nD_decoder
            self.KparDiff = [self.KparDiff[0]] * self.nD_encoder + [self.KparDiff[1]] * self.nD_decoder

            self.layer_A_indices = [i for i, val in enumerate(self.layers) if val == 'A']
            self.layer_D_indices = [i for i, val in enumerate(self.layers) if val == 'D']
            self.plot_layer = [self.nD_encoder-1, self.nD_encoder]
            self.removeDimLayer_In = [[self.nD_encoder], [self.removeDimLayer_In[1][0]]]
            self.addDimLayer_In = [[self.nD_encoder], [self.addDimLayer_In[1][0]]]
            self.createPatches = [False] * self.numLayers
            self.freezeLayer = [False for layer in range(self.numLayers)]
            self.NTr_layer = [self.NTr] * self.numLayers
            if self.cTr is not None:
                self.cTr_layer = [self.cTr] * self.numLayers
            self.trainSubset_flag = [False] * self.nD

            self.nControlPts = [self.NTr_layer[self.layer_D_indices[layer_D]] for layer_D in range(self.nD)]
            self.idx_diff = [[np.arange(self.NTr_layer[self.layer_D_indices[layer_D]])] for layer_D in range(self.nD)]
            self.idx_diff_shifted = self.idx_diff
            self.idx_diff_nodiff = self.idx_diff
            self.idx_nodiff = [[None] for layer_D in range(self.nD)]
            self.idx_nodiff_shifted = [[None] for layer_D in range(self.nD)]
            self.idx_controls = [[np.arange(self.nControlPts[layer_D])] for layer_D in range(self.nD)]
            self.N_controls = [self.nControlPts[layer_D] for layer_D in range(self.nD)]
            print('self.nD = ', self.nD)
            print('self.dim_D = ', self.dim_D)
            print('self.layers = ', self.layers)
            print('self.removeDimLayer_In = ', self.removeDimLayer_In)
            print('self.addDimLayer_In = ', self.addDimLayer_In)
            logging.info('nControlPts = {0}'.format(self.nControlPts))
            logging.info('N_controls = {0}'.format(self.N_controls))

            self.clamp = list(range(1,self.nD-1))
            print('self.clamp = ', self.clamp)
            self.idx_clamp = [None] * self.nD
            for patchIndex in range(1,self.nD_encoder):
                if patchIndex == 1:
                    self.nested_points = self.points_in_patch[0]
                else:
                    self.nested_points = np.concatenate((self.nested_points, self.points_in_patch[patchIndex-1]))
                self.idx_clamp[patchIndex] = np.copy(self.nested_points)
            for patchIndex in range(self.nD_decoder-1):
                self.idx_clamp[self.nD_encoder+patchIndex] = self.idx_clamp[self.nD_encoder-patchIndex-1]
            for layer_D in range(self.nD):
                if self.idx_clamp[layer_D] is None:
                    print(layer_D, self.idx_clamp[layer_D])
                else:
                    print(layer_D, self.idx_clamp[layer_D].size)
            self.nPatches = 1


        # initialize plots, algorithm, and variables -------------------------------------------------------------------
        if self.plot_flag or self.saveTraj_flag:
            if (self.cTr is None) and (self.plot_ind > self.dim_Y):
                logging.info('\nself.plot_ind must be <= self.dim_Y')
                exit()
        if self.plot_flag and (not self.plot_layer):
            self.plot_layer = [*range(self.numLayers)]

        self.plots_are_initialized = False

        self.resetAlgorithm(self.alg)

        self.iter = 0

        if self.vf_type == 'LDDMM':
            self.affB = [None] * self.nD
            self.affineDim = [0] * self.nD
            self.affineBasis = [None] * self.nD
            for layer_D in range(self.nD):
                if (self.affine[layer_D] == 'euclidean') and (self.dim_D[layer_D] < 3):
                    logging.info('\ncannot use affine = euclidean for dim_D < 3')
                    exit()
                self.affB[layer_D] = AffineBasis(self.dim_D[layer_D], self.affine[layer_D])
                self.affineDim[layer_D] = self.affB[layer_D].affineDim
                self.affineBasis[layer_D] = self.affB[layer_D].basis
                aw = 1.  # self.dim_D[layer_D] #np.sqrt(self.dim_D[layer_D]) ?
                self.affineWeight[layer_D] = self.affineWeight[layer_D] * np.ones([self.affineDim[layer_D], 1]) / aw
                if (len(self.affB[layer_D].rotComp) > 0) & (self.rotWeight[layer_D] != None):
                    self.affineWeight[layer_D][self.affB[layer_D].rotComp] = self.rotWeight[layer_D] / aw
                if (len(self.affB[layer_D].simComp) > 0) & (self.scaleWeight[layer_D] != None):
                    self.affineWeight[layer_D][self.affB[layer_D].simComp] = self.scaleWeight[layer_D] / aw
                if (len(self.affB[layer_D].transComp) > 0):
                    if (self.transWeight[layer_D] != None):
                        self.affineWeight[layer_D][self.affB[layer_D].transComp] = self.transWeight[layer_D]
                    else:
                        self.affineWeight[layer_D][self.affB[layer_D].transComp] *= aw

        if self.vf_type == 'LDDMM':
            self.at = [self.at_initFactor[layer_D] * np.random.normal(size=(self.Tsize[layer_D], self.N_controls[layer_D], self.dim_D[layer_D]))
                       for layer_D in range(self.nD)]
            self.Afft = [np.zeros((self.nPatches, self.Tsize[layer_D], self.affineDim[layer_D]))
                         if self.layer_D_indices[layer_D] in self.patchesLayer
                         else np.zeros((1, self.Tsize[layer_D], self.affineDim[layer_D]))
                         for layer_D in range(self.nD)]
            self.Kdtype = []
            for K in self.KparDiff:
                self.Kdtype.append(K.dtype)
            self.v = [np.zeros((self.Tsize[layer_D] + 1, self.N_controls[layer_D], self.dim_D[layer_D])) for layer_D in range(self.nD)]
        elif self.vf_type == 'NN':
            self.Wt = [None] * self.nD
            self.bt = [None] * self.nD
            for layer_D in range(self.nD):
                self.Wt[layer_D] = self.Wt_initFactor[layer_D] * np.random.normal(
                    size=(self.Tsize[layer_D], self.dim_D[layer_D], self.dim_D[layer_D]))
                self.bt[layer_D] = np.zeros((self.Tsize[layer_D], self.dim_D[layer_D]))

        self.initializeAffine()

        self.obj = None
        self.u_sVals = [None] * self.nA  # singular values of u matrices
        self.obj_array = np.array([])
        self.u_sVals_array = [np.array([])] * self.nA
        self.trainError_array = np.array([])


        # initialize trajectories --------------------------------------------------------------------------------------
        if self.vf_type == 'LDDMM':
            obj, self.zt, self.forwardState_In, self.forwardState_Out = self.objectiveFunDef_LDDMM(self.at, self.Afft, withTrajectory=True)
        elif self.vf_type == 'NN':
            obj, self.zt, self.forwardState_In, self.forwardState_Out = self.objectiveFunDef_NN(self.Wt, self.bt, withTrajectory=True)

        if self.model == 'regression':
            self.guTr = np.copy(self.forwardState_Out[-1][:, :self.dim_Y])
        elif self.model == 'classification':
            self.guTr = np.argmax(self.forwardState_Out[-1], axis=1)[:, np.newaxis]
        logging.info('-' * 121)
        if not self.plots_are_initialized:
            self.first_plot()


        # start fit ----------------------------------------------------------------------------------------------------
        # update_sigmaError
        if self.approach == 0:
            if self.update_sigmaError:
                if self.model == 'regression':
                    target_train_err = max(_s.sum(), 0.01)
                elif self.model == 'classification':
                    target_train_err = max(min(_s, 0.1), 0.01)
                logging.info(f'Target training error: {target_train_err:0.6f}')
                objEps_ = np.copy(self.objEps)
                maxIter_ = np.copy(self.maxIter)
                self.objEps = 1e-4
                self.maxIter = 2000
                self.optimizeMatching()
                nloop = 10
                loop = 1
                while loop < nloop and self.train_err > target_train_err:
                    self.sigmaError /= 1.4
                    logging.info(f'sigmaError = {self.sigmaError:.6f}')
                    self.resetAlgorithm(self.alg)
                    self.optimizeMatching()
                    loop += 1
                self.maxIter = maxIter_
                self.objEps = objEps_
                self.resetAlgorithm(self.alg)
            self.optimizeMatching()
        elif self.approach == 1:
            objEps_ = np.copy(self.objEps)
            maxIter_ = np.copy(self.maxIter)
            self.objEps = 1e-4
            self.optimizeMatching()
            nloop = self.nLoops
            for patchIndex in range(1,nloop):
                logging.info('\nPatch ' + str(patchIndex) + '\n')
                # self.maxIter += 200
                self.previous_nested_points = np.copy(self.nested_points)
                # self.new_points = np.setdiff1d(self.points_in_patch[patchIndex],self.nested_points)
                self.new_points = np.copy(self.points_in_patch[patchIndex])
                print(np.setdiff1d(self.points_in_patch[patchIndex],self.nested_points).size, self.new_points.size)
                self.previous_nested_points_local = np.arange(self.previous_nested_points.size)
                self.new_points_local = np.arange(self.new_points.size) + self.previous_nested_points.size
                self.idx_clamp = [np.copy(self.previous_nested_points_local) if layer_D in self.clamp else None for layer_D in range(self.nD)]

                # if patchIndex == 1:
                #     layer_A=0
                #     self.XTr = self.affineMult[layer_A](self.XTr, self.u[layer_A], self.b[layer_A])
                #     self.XTr_orig_transformed = self.affineMult[layer_A](self.XTr_orig_transformed, self.u[layer_A], self.b[layer_A])
                #     self.freezeLayer[0] = True
                if self.vf_type == 'LDDMM':
                    A = self.getAffine()
                    layer_D = 0

                    testRes = evol.landmarkDirectEvolutionEuler(self.XTr,
                                                                self.at[layer_D][:, self.idx_controls[layer_D][0], :],
                                                                self.KparDiff[layer_D],
                                                                affine=A[layer_D][0],
                                                                withPointSet=self.XTr_orig_transformed)
                                                                # if onlyPointSet=True, testRes[0] is not propagated
                                                                # but instead set to optional parameter zt
                    print('difference sum = ', np.sum(self.forwardState_Out[0]-testRes[0][-1,:,:]))
                    print('sum of self.forwardState_Out[0][:,-3] = ', np.sum(self.forwardState_Out[0][:,-3]))
                    print('sum of self.forwardState_Out[0][:,-2] = ', np.sum(self.forwardState_Out[0][:,-2]))
                    print('sum of self.forwardState_Out[0][:,-1] = ', np.sum(self.forwardState_Out[0][:,-1]))
                    self.XTr = np.concatenate((testRes[0][-1,:,:],testRes[1][-1,self.new_points,:]),axis=0)
                    self.XTr_orig_transformed = np.copy(testRes[1][-1,:,:])
                    print('difference sum2 = ', np.sum(testRes[0][:,:,:]-testRes[1][:,self.nested_points,:]))

                    # ##########################################################################
                    # layer_D=1
                    # testRes = evol.landmarkDirectEvolutionEuler(self.forwardState_Out[1],
                    #                                             -np.flip(self.at[layer_D][:, self.idx_controls[layer_D][0], :],axis=0),
                    #                                             self.KparDiff[layer_D],
                    #                                             affine=A[layer_D][0],
                    #                                             withPointSet=self.YTr_orig_transformed)
                    # # self.YTr = np.concatenate((testRes[0][-1, :, :], testRes[1][-1, self.new_points, :]), axis=0)
                    # self.YTr_orig_transformed = np.copy(testRes[1][-1,:,:])
                    # ##########################################################################

                self.nested_points = np.concatenate((self.nested_points, self.new_points))
                self.NTr_layer = [self.nested_points.size] * self.numLayers
                self.YTr_layer = [self.YTr_orig_transformed[self.nested_points, :]] * self.numLayers

                if self.cTr is not None:
                    self.cTr = self.cTr_orig[self.nested_points]
                    self.colors = np.copy(self.cTr)
                    self.cTr_layer = [self.cTr] * self.numLayers
                    self.colors_layer = [np.copy(self.cTr_layer[layer]) for layer in range(self.numLayers)]
                self.x3_orig_train = ps.build3DProjection(self.XTr_orig[self.nested_points, :self.dim_X], Y=self.Y, mode=self.model)
                self.nControlPts = [self.NTr_layer[self.layer_D_indices[layer_D]] for layer_D in range(self.nD)]
                self.idx_diff = [[np.arange(self.NTr_layer[self.layer_D_indices[layer_D]])] for layer_D in range(self.nD)]
                self.idx_diff_shifted = self.idx_diff
                self.idx_diff_nodiff = self.idx_diff
                self.idx_controls = [[np.arange(self.nControlPts[layer_D])] for layer_D in range(self.nD)]
                self.N_controls = [self.nControlPts[layer_D] for layer_D in range(self.nD)]
                logging.info('nControlPts = {0}'.format(self.nControlPts))
                logging.info('N_controls = {0}'.format(self.N_controls))

                self.at = [self.at_initFactor[layer_D] * np.random.normal(
                    size=(self.Tsize[layer_D], self.N_controls[layer_D], self.dim_D[layer_D]))
                           for layer_D in range(self.nD)]
                self.Afft = [np.zeros((self.nPatches, self.Tsize[layer_D], self.affineDim[layer_D]))
                             if self.layer_D_indices[layer_D] in self.patchesLayer
                             else np.zeros((1, self.Tsize[layer_D], self.affineDim[layer_D]))
                             for layer_D in range(self.nD)]
                self.v = [np.zeros((self.Tsize[layer_D] + 1, self.N_controls[layer_D], self.dim_D[layer_D])) for layer_D
                          in range(self.nD)]
                # self.initializeAffine()
                # self.obj = None
                obj, self.zt, self.forwardState_In, self.forwardState_Out = self.objectiveFunDef_LDDMM(self.at,
                                                                                                       self.Afft,
                                                                                                       withTrajectory=True)
                self.resetAlgorithm(self.alg)
                self.optimizeMatching()

            self.resetAlgorithm(self.alg)
            self.sigmaError = 0.01
            self.maxIter = 1000
            self.optimizeMatching()
        elif self.approach == 2:
            self.optimizeMatching()


        # close logging file
        logger = logging.getLogger()
        for h in logger.handlers:
            logger.removeHandler(h)


    def initializeAffine(self):
        # modify u_initFactor if Xavier_flag == True -------------------------------------------------------------------
        for layer_A in range(self.nA):
            if self.Xavier_flag[layer_A]:
                d1 = self.dim_A[layer_A][0] - 1
                self.u_initFactor[layer_A] = np.sqrt(1. / d1)
        if any(self.Xavier_flag):
            logging.info('u_initFactor = {0}'.format([float("%.5f" % item) for item in self.u_initFactor]))
            logging.info('-' * 121)
        self.u = [None] * self.nA
        self.b = [np.zeros((1, self.dim_A[layer_A][1])) for layer_A in range(self.nA)]
        self.ridgecost = [None] * self.nA
        self.ridgecostgradient = [None] * self.nA
        self.affineMult = [None] * self.nA
        self.affineBackMult = [None] * self.nA
        self.affineBackPred = [None] * self.nA
        for layer_A in range(self.nA):
            if self.ridgeType[layer_A] == 'reg':
                self.ridgecost[layer_A] = ps._RidgeSumOfSquares
                self.ridgecostgradient[layer_A] = ps._RidgeSumOfSquaresGradient
                self.affineMult[layer_A] = ps._GeneralAffineMult
                self.affineBackMult[layer_A] = ps._GeneralAffineBackMult
                self.affineBackPred[layer_A] = ps._GeneralAffineBackPred
                self.u[layer_A] = self.u_initFactor[layer_A] * np.random.normal(size=(self.dim_A[layer_A][0] - 1,
                                                                                      self.dim_A[layer_A][1]))
            elif self.ridgeType[layer_A] == 'diag':
                self.ridgecost[layer_A] = ps._RidgeSumOfSquaresDiag
                self.ridgecostgradient[layer_A] = ps._RidgeSumOfSquaresDiagGradient
                self.affineMult[layer_A] = ps._DiagAffineMult
                self.affineBackMult[layer_A] = ps._DiagAffineBackMult
                self.affineBackPred[layer_A] = ps._DiagAffineBackPred
                self.u[layer_A] = self.u_initFactor[layer_A] * np.random.normal(size=(1, self.dim_A[layer_A][0] - 1))
            elif self.ridgeType[layer_A] == 'diag_eye':
                self.ridgecost[layer_A] = ps._RidgeSumOfSquaresDiagToEye
                self.ridgecostgradient[layer_A] = ps._RidgeSumOfSquaresDiagToEyeGradient
                self.affineMult[layer_A] = ps._DiagAffineMult
                self.affineBackMult[layer_A] = ps._DiagAffineBackMult
                self.affineBackPred[layer_A] = ps._DiagAffineBackPred
                self.u[layer_A] = self.u_initFactor[layer_A] * np.random.normal(
                    size=(1, self.dim_A[layer_A][0] - 1))
                self.u[layer_A] += np.ones(self.u[layer_A].shape)
            elif self.ridgeType[layer_A] == 'scalar':
                self.ridgecost[layer_A] = ps._RidgeSumOfSquaresScalar
                self.ridgecostgradient[layer_A] = ps._RidgeSumOfSquaresScalarGradient
                self.affineMult[layer_A] = ps._ScalarAffineMult
                self.affineBackMult[layer_A] = ps._ScalarAffineBackMult
                self.affineBackPred[layer_A] = ps._ScalarAffineBackPred
                self.u[layer_A] = self.u_initFactor[layer_A] * np.random.normal(size=(1, 1))
            elif self.ridgeType[layer_A] == 'eye':
                self.ridgecost[layer_A] = ps._RidgeSumOfSquaresToEye
                self.ridgecostgradient[layer_A] = ps._RidgeSumOfSquaresToEyeGradient
                self.affineMult[layer_A] = ps._GeneralAffineMult
                self.affineBackMult[layer_A] = ps._GeneralAffineBackMult
                self.affineBackPred[layer_A] = ps._GeneralAffineBackPred
                self.u[layer_A] = self.u_initFactor[layer_A] * np.random.normal(size=(self.dim_A[layer_A][0] - 1,
                                                                                      self.dim_A[layer_A][1]))
                self.u[layer_A] = self.u[layer_A] * np.eye(self.dim_A[layer_A][0] - 1, self.dim_A[layer_A][1])
        if self.model == 'classification':
            self.u[-1][:, 0] = 0


    def predict(self, predict_x, predict_y=None):
        if self.logging_flag:
            loggingUtils.setup_default_logging(self.outputDir, fileName=self.loggingFile, stdOutput=True, mode='a')  # 'a' = write without overwriting
        else:
            loggingUtils.setup_default_logging(stdOutput=False)

        self.XPr = np.copy(predict_x)
        self.NPr = predict_x.shape[0]
        self.ztPr = [None] * self.nD
        self.forwardStatePr_In = [None] * self.numLayers
        self.forwardStatePr_Out = [None] * self.numLayers
        for layer_D in range(self.nD):
            if len(self.idx_diff[layer_D]) > 1:
                logging.info('\ncannot do prediction if patches were created during training')
                exit()
        if self.standardize_fit_x:
            self.XPr = np.divide(self.XPr - self.mu0_x, self.s0_x)
        if self.addDim_X > 0:
            self.XPr = np.concatenate((self.XPr, np.zeros((self.NPr, self.addDim_X))), axis=1)
        if self.vf_type == 'LDDMM':
            A = self.getAffine()
        zPr = np.copy(self.XPr)
        for layer in range(self.numLayers):
            if layer in self.removeDimLayer_In[0]:
                zPr = np.delete(zPr, self.removeDimLayer_In[1][self.removeDimLayer_In[0].index(layer)], axis=1)
            if layer in self.addDimLayer_In[0]:
                zPr = ps.insertZeroColumns(zPr, self.addDimLayer_In[1][self.addDimLayer_In[0].index(layer)])
            self.forwardStatePr_In[layer] = np.copy(zPr)
            if self.layers[layer] == 'D':
                layer_D = self.layer_D_indices.index(layer)
                if self.vf_type == 'LDDMM':
                    testRes = evol.landmarkDirectEvolutionEuler(self.zt[layer_D][0, self.idx_diff[layer_D][0], :],
                                                                self.at[layer_D][:, self.idx_controls[layer_D][0],:],
                                                                self.KparDiff[layer_D],
                                                                affine=A[layer_D][0],
                                                                withPointSet=zPr,
                                                                onlyPointSet=True,
                                                                zt=np.copy(self.zt[layer_D][:, self.idx_diff_nodiff[layer_D][0], :]))
                                                                # if onlyPointSet=True, testRes[0] is not propagated
                                                                # but instead set to optional parameter zt
                    self.ztPr[layer_D] = np.copy(testRes[1])
                elif self.vf_type == 'NN':
                    self.ztPr[layer_D] = evol.NNEvolution(zPr, self.Wt[layer_D], self.bt[layer_D],
                                                          fun=self.fun[layer_D])
                zPr = np.copy(self.ztPr[layer_D][-1, :, :])
            else:
                layer_A = self.layer_A_indices.index(layer)
                zPr = self.affineMult[layer_A](zPr, self.u[layer_A], self.b[layer_A])
            self.forwardStatePr_Out[layer] = np.copy(zPr)
        if self.model == 'regression':
            self.guPr = np.copy(self.forwardStatePr_Out[-1][:, :self.dim_Y])
        elif self.model == 'classification':
            self.guPr = np.argmax(self.forwardStatePr_Out[-1], axis=1)[:, np.newaxis]

        self.predict_results = np.copy(self.guPr)
        if (self.model == 'regression') and (self.standardize_fit_y):
            self.predict_results = np.multiply(self.predict_results, self.s0_y) + self.mu0_y

        if predict_y is not None:
            logging.info('-' * 121)
            if self.model == 'regression':
                self.predict_err = ((self.predict_results - predict_y) ** 2).sum() / predict_y.shape[0]
            elif self.model == 'classification':
                nPr = np.array([(predict_y == k).sum() for k in range(self.nClasses)])
                wPr = float(predict_y.size) / (nPr[predict_y[:, 0]] * self.nClasses)[:, np.newaxis]
                swPr = wPr.sum()
                self.predict_err = np.sum(np.not_equal(self.predict_results, predict_y) * wPr) / swPr
            logging.info('Prediction Error {0:.8f}'.format(self.predict_err))

        # close logging file
        logger = logging.getLogger()
        for h in logger.handlers:
            logger.removeHandler(h)

        return self.predict_results

    ####################################################################################################################
    #   diffeoLearn stop
    ####################################################################################################################

    ####################################################################################################################
    #   optimizeMatching start
    ####################################################################################################################

    def optimizeMatching(self):
        self.coeffAff = self.coeffAff2
        grd = self.getGradient(self.gradCoeff)
        if self.alg == 'bfgs':
            [grd2] = self.dotProduct_euclidean(grd, [grd])
        else:
            [grd2] = self.dotProduct(grd, [grd])
        if self.gradEps < 0:
            self.gradEps = max(0.00001, np.sqrt(grd2) / 10000000)
            # self.gradEps = max(0.0001, np.sqrt(grd2) / 10000)
        self.coeffAff = self.coeffAff1
        if self.alg == 'bfgs':
            bfgs.bfgs(self, verb=self.verb, maxIter=self.maxIter, TestGradient=self.testGradient,
                      epsInit=self.epsInit, memory=self.memory, Wolfe=self.Wolfe)
        else:
            cg.cg(self, verb=self.verb, maxIter=self.maxIter, TestGradient=self.testGradient,
                  epsInit=self.epsInit)


    def resetAlgorithm(self, algorithm):
        if algorithm == 'cg':
            self.alg = 'cg'
            for layer_D in range(self.nD):
                if self.KparDiff[layer_D].localMaps in (None, 'predict'):
                    self.coeffuPred = 1.    # value for all A layers
                    self.coeffAff1 = 1.
                    self.coeffAff2 = 1.
                else:
                    self.coeffuPred = 1.    # value for all A layers
                    self.coeffAff1 = 1.
                    self.coeffAff2 = 1.
            self.coeffAff = self.coeffAff1  # value for all D layers (for vf_type == 'LDDMM')
            if self.Wolfe:
                self.euclideanGradient = [True] * self.nD
            else:
                self.euclideanGradient = [False] * self.nD
        else:
            self.alg = 'bfgs'
            self.coeffuPred = 1.            # value for all A layers
            self.coeffAff1 = 1.
            self.coeffAff2 = 1.
            self.coeffAff = self.coeffAff1  # value for all D layers (for vf_type == 'LDDMM')
            self.euclideanGradient = [True] * self.nD
        for layer_D in range(self.nD):
            if self.trainSubset_flag[layer_D]:
                self.euclideanGradient[layer_D] = True
        self.reset = True

    ####################################################################################################################
    #   optimizeMatching stop
    ####################################################################################################################

    ####################################################################################################################
    #   gradientTools start
    ####################################################################################################################

    def testGradientFun(self, obj, grd, gradCoeff, opt=None, dotProduct=None):
        dirfoo = opt.randomDir()
        epsfoo = 1e-4  # 1e-6 (in bfgs)
        objfoo1 = opt.updateTry(dirfoo, epsfoo, obj - 1e10, log=False)
        [grdfoo] = dotProduct(grd, [dirfoo])
        ## logging.info('Test Gradient: %.6f %.6f' %((objfoo - obj)/epsfoo, -grdfoo * gradCoeff))
        objfoo1_ = opt.updateTry(dirfoo, -epsfoo, obj - 1e10, log=False)
        # logging.info('Test Gradient: %.6f %.6f' % ((objfoo1 - objfoo1_) / (2.0 * epsfoo), -grdfoo * gradCoeff))
        objfoo2 = opt.updateTry(dirfoo, 2.0 * epsfoo, obj - 1e10, log=False)
        objfoo2_ = opt.updateTry(dirfoo, -2.0 * epsfoo, obj - 1e10, log=False)
        # logging.info('Test Gradient: %.6f %.6f' % ((-objfoo2 + 8.0 * objfoo1 - 8.0 * objfoo1_ + objfoo2_) / (12.0 * epsfoo), -grdfoo * gradCoeff))
        objfoo3 = opt.updateTry(dirfoo, 3.0 * epsfoo, obj - 1e10, log=False)
        objfoo3_ = opt.updateTry(dirfoo, -3.0 * epsfoo, obj - 1e10, log=False)
        # logging.info('Test Gradient: %.6f %.6f' % ((objfoo3 - 9.0 * objfoo2 + 45.0 * objfoo1 - 45.0 * objfoo1_ + 9.0 * objfoo2_ - objfoo3_) / (60.0 * epsfoo), -grdfoo * gradCoeff))
        objfoo4 = opt.updateTry(dirfoo, 4.0 * epsfoo, obj - 1e10, log=False)
        objfoo4_ = opt.updateTry(dirfoo, -4.0 * epsfoo, obj - 1e10, log=False)
        logging.info('Test Gradient (all free):     %.6f %.6f' % ((-objfoo4 + 32.0 / 3.0 * objfoo3 - 56.0 * objfoo2 + 224.0 * objfoo1 - 224.0 * objfoo1_ + 56.0 * objfoo2_ - 32.0 / 3.0 * objfoo3_ + objfoo4_) / (280.0 * epsfoo), -grdfoo * gradCoeff))

        epsfoo = 1e-4  # 1e-6 (in bfgs)
        for cases in range(self.nA + self.nD):
            dirfoo_fixed = self.copyDir(dirfoo)
            dirfoo_fixed_copy = self.copyDir(dirfoo)
            dirfoo_fixed.upred = [np.zeros(shape=dirfoo_fixed.upred[layer_A].shape) for layer_A in range(self.nA)]
            dirfoo_fixed.bpred = [np.zeros(shape=dirfoo_fixed.bpred[layer_A].shape) for layer_A in range(self.nA)]
            if self.vf_type == 'LDDMM':
                dirfoo_fixed.diff = [np.zeros(shape=dirfoo_fixed.diff[layer_D].shape) for layer_D in range(self.nD)]
                dirfoo_fixed.aff = [np.zeros(shape=dirfoo_fixed.aff[layer_D].shape) for layer_D in range(self.nD)]
            elif self.vf_type == 'NN':
                dirfoo_fixed.W = [np.zeros(shape=dirfoo_fixed.W[layer_D].shape) for layer_D in range(self.nD)]
                dirfoo_fixed.b = [np.zeros(shape=dirfoo_fixed.b[layer_D].shape) for layer_D in range(self.nD)]
            if cases < self.nA:
                layer_A = cases
                fixed_str = '(ub' + str(layer_A) + ' free):'
                dirfoo_fixed.upred[layer_A] = np.copy(dirfoo_fixed_copy.upred[layer_A])
                dirfoo_fixed.bpred[layer_A] = np.copy(dirfoo_fixed_copy.bpred[layer_A])
            elif cases < self.nA + self.nD:
                layer_D = cases-self.nA
                fixed_str = '(vf' + str(layer_D) + ' free):'
                if self.vf_type == 'LDDMM':
                    dirfoo_fixed.diff[layer_D] = np.copy(dirfoo_fixed_copy.diff[layer_D])
                    dirfoo_fixed.aff[layer_D] = np.copy(dirfoo_fixed_copy.aff[layer_D])
                elif self.vf_type == 'NN':
                    dirfoo_fixed.W[layer_D] = np.copy(dirfoo_fixed_copy.W[layer_D])
                    dirfoo_fixed.b[layer_D] = np.copy(dirfoo_fixed_copy.b[layer_D])
            objfoo1 = opt.updateTry(dirfoo_fixed, epsfoo, obj - 1e10, log=False)
            [grdfoo] = dotProduct(grd, [dirfoo_fixed])
            ## logging.info('Test Gradient %s     %.6f %.6f' %(fixed_str, (objfoo - obj)/epsfoo, -grdfoo * gradCoeff))
            objfoo1_ = opt.updateTry(dirfoo_fixed, -epsfoo, obj - 1e10, log=False)
            # logging.info('Test Gradient %s     %.6f %.6f' % (fixed_str, (objfoo1 - objfoo1_) / (2.0 * epsfoo), -grdfoo * gradCoeff))
            objfoo2 = opt.updateTry(dirfoo_fixed, 2.0 * epsfoo, obj - 1e10, log=False)
            objfoo2_ = opt.updateTry(dirfoo_fixed, -2.0 * epsfoo, obj - 1e10, log=False)
            # logging.info('Test Gradient %s     %.6f %.6f' % (fixed_str, (-objfoo2 + 8.0 * objfoo1 - 8.0 * objfoo1_ + objfoo2_) / (12.0 * epsfoo), -grdfoo * gradCoeff))
            objfoo3 = opt.updateTry(dirfoo_fixed, 3.0 * epsfoo, obj - 1e10, log=False)
            objfoo3_ = opt.updateTry(dirfoo_fixed, -3.0 * epsfoo, obj - 1e10, log=False)
            # logging.info('Test Gradient %s     %.6f %.6f' % (fixed_str, (objfoo3 - 9.0 * objfoo2 + 45.0 * objfoo1 - 45.0 * objfoo1_ + 9.0 * objfoo2_ - objfoo3_) / (60.0 * epsfoo), -grdfoo * gradCoeff))
            objfoo4 = opt.updateTry(dirfoo_fixed, 4.0 * epsfoo, obj - 1e10, log=False)
            objfoo4_ = opt.updateTry(dirfoo_fixed, -4.0 * epsfoo, obj - 1e10, log=False)
            logging.info('Test Gradient %s     %.6f %.6f' % (fixed_str, (-objfoo4 + 32.0 / 3.0 * objfoo3 - 56.0 * objfoo2 + 224.0 * objfoo1 - 224.0 * objfoo1_ + 56.0 * objfoo2_ - 32.0 / 3.0 * objfoo3_ + objfoo4_) / (280.0 * epsfoo), -grdfoo * gradCoeff))


    def getGradient(self, coeff=1.0, update=None):
                                                # called by optimizeMatching, cg, and bfgs
                                                # returns grd.upred, grd.bpred, grd.diff, and grd.aff
                                                # getGradient(coeff) returns coeff * gradient; the result can be used as 'direction' in updateTry
                                                # (if update is not None, updated form = [u, b, zt, forwardState_In, forwardState_Out, at, Afft, dir, eps]
                                                #                                    or = [u, b, zt, forwardState_In, forwardState_Out, Wt, bt, dir, eps])
                                                # (note:  zt, forwardState_In, and forwardState_Out are NOT variables;
                                                #         however, for bfgs, providing them in the 'updated' parameters
                                                #         allows us to bypass unnecessary propagation)
        if update is None:       # True for cg and parts of bfgs
            u = copy.deepcopy(self.u)
            b = copy.deepcopy(self.b)
            zt = copy.deepcopy(self.zt)
            forwardState_In = copy.deepcopy(self.forwardState_In)
            forwardState_Out = copy.deepcopy(self.forwardState_Out)
            if self.vf_type == 'LDDMM':
                at = copy.deepcopy(self.at)
                Afft = copy.deepcopy(self.Afft)
                A = self.getAffine(Afft=Afft)
            elif self.vf_type == 'NN':
                Wt = copy.deepcopy(self.Wt)
                bt = copy.deepcopy(self.bt)
        else:                   # only in bfgs for saving intermediate tries during line search
            if (update[0] is self.updated_lineSearch[7]) and (update[1] == self.updated_lineSearch[8]):
                updated = self.updated_lineSearch
                u = copy.deepcopy(updated[0])
                b = copy.deepcopy(updated[1])
                zt = copy.deepcopy(updated[2])
                forwardState_In = copy.deepcopy(updated[3])
                forwardState_Out = copy.deepcopy(updated[4])
                if self.vf_type == 'LDDMM':
                    at = copy.deepcopy(updated[5])
                    Afft = copy.deepcopy(updated[6])
                    A = self.getAffine(Afft=Afft)
                elif self.vf_type == 'NN':
                    Wt = copy.deepcopy(updated[5])
                    bt = copy.deepcopy(updated[6])
            else:
                dir = update[0]
                eps = update[1]
                uTry = [(self.u[layer_A] - eps * dir.upred[layer_A]) for layer_A in range(self.nA)]
                bTry = [(self.b[layer_A] - eps * dir.bpred[layer_A]) for layer_A in range(self.nA)]
                if self.vf_type == 'LDDMM':
                    atTry = [(self.at[layer_D] - eps * dir.diff[layer_D]) for layer_D in range(self.nD)]
                    AfftTry = copy.deepcopy(self.Afft)
                    AfftTry = [(self.Afft[layer_D] - eps * dir.aff[layer_D]) if self.affineDim[layer_D] > 0 else AfftTry[layer_D] for layer_D in range(self.nD)]
                    foo = self.objectiveFunDef_LDDMM(atTry, AfftTry, u=uTry, b=bTry, withTrajectory=True)
                    at = copy.deepcopy(atTry)
                    Afft = copy.deepcopy(AfftTry)
                    A = self.getAffine(Afft=Afft)
                elif self.vf_type == 'NN':
                    WtTry = [(self.Wt[layer_D] - eps * dir.W[layer_D]) for layer_D in range(self.nD)]
                    btTry = [(self.bt[layer_D] - eps * dir.b[layer_D]) for layer_D in range(self.nD)]
                    foo = self.objectiveFunDef_NN(WtTry, btTry, u=uTry, b=bTry, withTrajectory=True)
                    Wt = copy.deepcopy(WtTry)
                    bt = copy.deepcopy(btTry)
                ztTry = copy.deepcopy(foo[1])
                forwardStateInTry = copy.deepcopy(foo[2])
                forwardStateOutTry = copy.deepcopy(foo[3])
                u = copy.deepcopy(uTry)
                b = copy.deepcopy(bTry)
                zt = copy.deepcopy(ztTry)
                forwardState_In = copy.deepcopy(forwardStateInTry)
                forwardState_Out = copy.deepcopy(forwardStateOutTry)

        backpropStateMisc_In, backpropStateMisc_Out = self.miscBackpropStates(forwardState_In, forwardState_Out)

        grd = Direction(vf_type=self.vf_type, nD=self.nD, nA=self.nA)

        # # calculate grd.upred and grd.bpred components from affine regularization term
        # grd.upred, grd.bpred = ps.RidgeGradientInU(u, b, lam=self.lam, ridgecostgradient=self.ridgecostgradient)

        # start backpropagation and calculation of grd.diff, grd.aff, and the rest of grd.upred and grd.bpred
        finalState = forwardState_Out[-1]
        backpropState_In = [None] * self.numLayers
        backpropState_Out = [None] * self.numLayers
        for layer_D in range(self.nD):
            backpropState_In[self.layer_D_indices[layer_D]] = np.zeros((self.NTr_layer[self.layer_D_indices[layer_D]], self.dim_D[layer_D]))
            backpropState_Out[self.layer_D_indices[layer_D]] = np.zeros((self.NTr_layer[self.layer_D_indices[layer_D]], self.dim_D[layer_D]))
        if self.model == 'regression':
            backpropState_In[-1] = -ps.RegressionScoreGradient(finalState, self.YTr_layer[-1], sigmaError=self.sigmaError)
        elif self.model == 'classification':
            backpropState_In[-1] = -ps.LogisticScoreGradient(finalState, self.YTr_layer[-1], w=self.wTr, sigmaError=self.sigmaError)

        for layer in range(self.numLayers-1,-1,-1):
            backpropState_In[layer] -= backpropStateMisc_In[layer]

            if self.layers[layer] == 'A':
                layer_A = self.layer_A_indices.index(layer)

                if not self.freezeLayer[layer]:

                    # calculate grd.upred and grd.bpred components from affine regularization term
                    grd.upred[layer_A], grd.bpred[layer_A] = ps.RidgeGradientInU(u[layer_A], b[layer_A], lam=self.lam[layer_A],
                                                                                 ridgecostgradient=self.ridgecostgradient[layer_A])

                    # calculate grd.upred and grd.bpred components of current layer
                    upred, bpred = self.affineBackPred[layer_A](forwardState_In[layer], backpropState_In[layer])

                    # combine grd.upred components and grd.bpred components
                    grd.upred[layer_A] += upred
                    grd.bpred[layer_A] += bpred
                    grd.upred[layer_A] = grd.upred[layer_A] / (coeff * self.coeffuPred)
                    grd.bpred[layer_A] = grd.bpred[layer_A] / (coeff * self.coeffuPred)  # double-check this
                    if not self.b_flag[layer_A]:
                        grd.bpred[layer_A] = np.zeros(self.b[layer_A].shape)

                    # calculate backpropState_Out of current layer
                    backpropState_Out[layer] = self.affineBackMult[layer_A](backpropState_In[layer], u[layer_A], b[layer_A])

                else:
                    grd.upred[layer_A] = np.zeros(u[layer_A].shape)
                    grd.bpred[layer_A] = np.zeros(b[layer_A].shape)
                    backpropState_Out[layer] = self.affineBackMult[layer_A](backpropState_In[layer], u[layer_A], b[layer_A])

            else:
                layer_D = self.layer_D_indices.index(layer)

                # calculate backpropState_Out of current layer, grd.diff, and grd.aff
                if self.vf_type == 'LDDMM':
                    grd.diff[layer_D] = np.zeros(self.at[layer_D].shape)
                    grd.aff[layer_D] = np.zeros(self.Afft[layer_D].shape)
                    for patch in range(len(self.idx_diff[layer_D])):
                        foo = self.hamiltonianGradient_LDDMM(backpropState_In[layer][self.idx_diff_nodiff[layer_D][patch], :],
                                                             at[layer_D][:,self.idx_controls[layer_D][patch],:], A[layer_D][patch],
                                                             self.KparDiff[layer_D], self.gam[layer_D],
                                                             x0=zt[layer_D][0, self.idx_diff_nodiff[layer_D][patch], :],
                                                             zt=zt[layer_D][:, self.idx_diff_nodiff[layer_D][patch], :],
                                                             idx_diff=self.idx_diff_shifted[layer_D][patch],
                                                             idx_nodiff=self.idx_nodiff_shifted[layer_D][patch],
                                                             idx_clamp=self.idx_clamp[layer_D], C=self.C)
                                                # hamiltonianGradient_LDDMM(...) = dat, xt, pxt (if affine=None and getCovector=True)
                                                # = dat, dA, db, xt, pxt (if affine not None and getCovector=True)
                                                # includes pt (by propagating p(1) backwards to p(0)) and dat (= p-2a)
                        if self.euclideanGradient[layer_D]:  # True for bfgs or trainSubset_flag[layer_D]=True
                            for t in range(self.Tsize[layer_D]):
                                z_ = zt[layer_D][t, self.idx_diff[layer_D][patch], :]
                                grd.diff[layer_D][t, self.idx_controls[layer_D][patch], :] = \
                                    self.KparDiff[layer_D].applyK(z_,foo[0][t, :, :]) / (coeff * self.Tsize[layer_D])
                                if self.trainSubset_flag[layer_D]:
                                    z__ = zt[layer_D][t, self.idx_nodiff[layer_D][patch], :]
                                    grd.diff[layer_D][t, self.idx_controls[layer_D][patch], :] -= \
                                        self.KparDiff[layer_D].applyK(z__,foo[-1][t+1, self.idx_nodiff_shifted[layer_D][patch], :],firstVar=z_) / (coeff * self.Tsize[layer_D])
                        else:  # True for cg with Wolfe=True and trainSubset_flag[layer_D]=False
                            grd.diff[layer_D][:,self.idx_controls[layer_D][patch],:] = foo[0] / (coeff * self.Tsize[layer_D])
                            exit()
                        dim2 = self.dim_D[layer_D] ** 2
                        if self.affineDim[layer_D] > 0:
                            dA = foo[1]
                            db = foo[2]
                            grd.aff[layer_D][patch,:,:] = 2 * self.affineWeight[layer_D].reshape([1, self.affineDim[layer_D]]) * Afft[layer_D][patch,:,:]
                            for t in range(self.Tsize[layer_D]):
                                dAff = self.affineBasis[layer_D].T.dot(np.vstack(
                                    [dA[t].reshape([dim2, 1]), db[t].reshape([self.dim_D[layer_D], 1])]))
                                grd.aff[layer_D][patch,t,:] -= dAff.reshape(grd.aff[layer_D][patch,t,:].shape)
                            grd.aff[layer_D][patch,:,:] /= (self.coeffAff * coeff * self.Tsize[layer_D])
                        if layer_D in self.clamp:
                            if self.idx_clamp[layer_D] is not None:
                                for t in range(self.Tsize[layer_D]):
                                    z_ = zt[layer_D][t, self.idx_diff[layer_D][patch], :]
                                    a_ = at[layer_D][t, self.idx_controls[layer_D][patch],:]
                                    ra_ = self.KparDiff[layer_D].applyK(z_, a_, firstVar = zt[layer_D][t, self.idx_clamp[layer_D], :])
                                    ra__ = self.KparDiff[layer_D].applyK(zt[layer_D][t, self.idx_clamp[layer_D],:], ra_,
                                                                         firstVar=z_)
                                    grd.diff[layer_D][t, self.idx_controls[layer_D][patch], :] += \
                                        2 * self.C * ra__ / (coeff * self.Tsize[layer_D])

                        backpropState_Out[layer][self.idx_diff_nodiff[layer_D][patch], :] = np.copy(foo[-1][0, :, :])  # foo[-1].shape = < Tsize[layer_D], NTr, dim_D[layer_D] >
                elif self.vf_type == 'NN':
                    foo = self.hamiltonianGradient_NN(backpropState_In[layer], Wt[layer_D], bt[layer_D], self.fun[layer_D],
                                                      self.weights[layer_D], x0=zt[layer_D][0, :, :], zt=zt[layer_D][:, :, :])
                                        # hamiltonianGradient_NN(...) = dWt, dbt, pxt
                    grd.W[layer_D] = foo[0] / (coeff * self.Tsize[layer_D])
                    grd.b[layer_D] = foo[1] / (coeff * self.Tsize[layer_D])

                    backpropState_Out[layer] = np.copy(foo[-1][0, :, :])  # foo[-1].shape = < Tsize[layer_D], NTr, dim_D[layer_D] >

                if self.freezeLayer[layer]:
                    grd.diff[layer_D] = np.zeros(self.at[layer_D].shape)
                    grd.aff[layer_D] = np.zeros(self.Afft[layer_D].shape)
                    # backpropState_Out[layer] = backpropState_In[layer]

            backpropState_Out[layer] -= backpropStateMisc_Out[layer]

            # calculate backpropState_In of (layer-1)
            if layer > 0:
                if self.createPatches[layer]:
                    backpropState_In_ = np.zeros(shape=(self.NTr_layer[layer - 1], backpropState_Out[layer].shape[1]))
                    for patch in range(self.nPatches):
                        backpropState_In_[self.points_in_patch[patch], :] += np.copy(backpropState_Out[layer][self.idx_patches[patch],:])
                else:
                    backpropState_In_ = np.copy(backpropState_Out[layer])

                if layer in self.addDimLayer_In[0]:
                    backpropState_In_ = np.delete(backpropState_In_, self.addDimLayer_In[1][self.addDimLayer_In[0].index(layer)], axis=1)
                if layer in self.removeDimLayer_In[0]:
                    backpropState_In_ = ps.insertZeroColumns(backpropState_In_, self.removeDimLayer_In[1][self.removeDimLayer_In[0].index(layer)])

                backpropState_In[layer - 1] = np.copy(backpropState_In_)

        return grd


    def miscBackpropStates(self, _forwardState_In, _forwardState_Out):
        backpropStateMisc_In = [0.0 for layer in range(self.numLayers)]
        backpropStateMisc_Out = [0.0 for layer in range(self.numLayers)]
        if self.repulse_flag:
            backpropStateMisc_In = list(map(add, backpropStateMisc_In,
                                            self.miscBackpropState(_forwardState_Out, self.repulseLayer_Out, ps.RepulseCostGradient, self.repulse_sigmaError_non_overlap,
                                                                   self.non_overlapping_patches, miscType='repulse', dist=2.0 * self.ball_eps)))
            backpropStateMisc_Out = list(map(add, backpropStateMisc_Out,
                                             self.miscBackpropState(_forwardState_In, self.repulseLayer_In, ps.RepulseCostGradient, self.repulse_sigmaError_non_overlap,
                                                                    self.non_overlapping_patches, miscType='repulse', dist=2.0 * self.ball_eps)))
            # backpropStateMisc_In = list(map(add, backpropStateMisc_In,
            #                                 self.miscBackpropState(_forwardState_Out, self.repulseLayer_Out, ps.RepulseCostGradient, self.repulse_sigmaError_overlap,
            #                                                        self.overlapping_patches, miscType='repulse', dist=1.0 * self.ball_eps)))
            # backpropStateMisc_Out = list(map(add, backpropStateMisc_Out,
            #                                  self.miscBackpropState(_forwardState_In, self.repulseLayer_In, ps.RepulseCostGradient, self.repulse_sigmaError_overlap,
            #                                                         self.overlapping_patches, miscType='repulse', dist=1.0 * self.ball_eps)))
        if self.elastic_flag:
            backpropStateMisc_In = list(map(add, backpropStateMisc_In,
                                            self.miscBackpropState(_forwardState_Out, self.elasticLayer_Out, ps.ElasticCostGradient, self.elastic_sigmaError,
                                                                   self.overlapping_patches_elastic, miscType='elastic')))
            backpropStateMisc_Out = list(map(add, backpropStateMisc_Out,
                                             self.miscBackpropState(_forwardState_In, self.elasticLayer_In, ps.ElasticCostGradient, self.elastic_sigmaError,
                                                                    self.overlapping_patches_elastic, miscType='elastic')))
        if self.elasticPatches_flag:
            backpropStateMisc_In = list(map(add, backpropStateMisc_In,
                                            self.miscBackpropState(_forwardState_Out, self.elasticPatchesLayer_Out, ps.ElasticCostGradient, self.elasticPatches_sigmaError,
                                                                   self.overlapping_patches, miscType='elasticPatches')))
            backpropStateMisc_Out = list(map(add, backpropStateMisc_Out,
                                             self.miscBackpropState(_forwardState_In, self.elasticPatchesLayer_In, ps.ElasticCostGradient, self.elasticPatches_sigmaError,
                                                                    self.overlapping_patches, miscType='elasticPatches')))
        if self.zeroFlatten_flag:
            backpropStateMisc_In = list(map(add, backpropStateMisc_In,
                                            self.miscBackpropState(_forwardState_Out, self.zeroFlattenLayer_Out, ps.ZeroFlattenCostGradient, self.zeroFlatten_sigmaError,
                                                                   None, miscType='zeroFlatten')))
            backpropStateMisc_Out = list(map(add, backpropStateMisc_Out,
                                             self.miscBackpropState(_forwardState_In, self.zeroFlattenLayer_In, ps.ZeroFlattenCostGradient, self.zeroFlatten_sigmaError,
                                                                    None, miscType='zeroFlatten')))
        if self.flatten_flag:
            backpropStateMisc_In = list(map(add, backpropStateMisc_In,
                                            self.miscBackpropState(_forwardState_Out, self.flattenLayer_Out, ps.FlattenCostGradient, self.flatten_sigmaError,
                                                                   self.overlapping_patches, miscType='flatten')))
            backpropStateMisc_Out = list(map(add, backpropStateMisc_Out,
                                             self.miscBackpropState(_forwardState_In, self.flattenLayer_In, ps.FlattenCostGradient, self.flatten_sigmaError,
                                                                    self.overlapping_patches, miscType='flatten')))
        if self.center_flag:
            backpropStateMisc_In = list(map(add, backpropStateMisc_In,
                                            self.miscBackpropState(_forwardState_Out, self.centerLayer_Out, ps.CostGradient, self.center_sigmaError,
                                                                   self.overlapping_patches, miscType='center')))
            backpropStateMisc_Out = list(map(add, backpropStateMisc_Out,
                                             self.miscBackpropState(_forwardState_In, self.centerLayer_In, ps.CostGradient, self.center_sigmaError,
                                                                    self.overlapping_patches, miscType='center')))
        if self.stitchPatches_flag:
            backpropStateMisc_In = list(map(add, backpropStateMisc_In,
                                            self.miscBackpropState(_forwardState_Out, self.stitchLayer_Out, ps.CostGradient, self.stitch_sigmaError,
                                                                   self.overlapping_patches, miscType='stitch')))
            backpropStateMisc_Out = list(map(add, backpropStateMisc_Out,
                                             self.miscBackpropState(_forwardState_In, self.stitchLayer_In, ps.CostGradient, self.stitch_sigmaError,
                                                                    self.overlapping_patches, miscType='stitch')))
        return backpropStateMisc_In, backpropStateMisc_Out


    def miscBackpropState(self, forwardState_, miscLayer_, MiscCostGradient, sigmaError_, patches, miscType='repulse', dist=1.0):
        backpropStateForce = [np.zeros(forwardState_[layer].shape) for layer in range(self.numLayers)]
        if miscType == 'elastic':
            centerIndices = self.centerIndices_elastic
            XTr_centerIndices = self.XTr_centerIndices_elastic
        elif miscType == 'elasticPatches':
            centerIndices = self.centerIndices_elasticPatches
            XTr_centerIndices = self.XTr_centerIndices_elasticPatches
        else:
            nPatches = self.nPatches
            centerIndices = self.centerIndices
            patch_centers = self.patch_centers
        for layer in range(self.numLayers):
            if layer in miscLayer_[0]:
                if miscType in ('elastic','elasticPatches'):
                    backpropForce = MiscCostGradient(forwardState_[layer][centerIndices[layer][0], :],
                                                     forwardState_[layer][centerIndices[layer][1], :],
                                                     self.XTr_orig[XTr_centerIndices[0], :self.dim_X],
                                                     self.XTr_orig[XTr_centerIndices[1], :self.dim_X],
                                                     sigmaError=sigmaError_,
                                                     dimensions=miscLayer_[1][miscLayer_[0].index(layer)])
                    for j in list(set(centerIndices[layer][0])):
                        backpropStateForce[layer][j, :] = backpropStateForce[layer][j, :] \
                                                          + (backpropForce[np.where(centerIndices[layer][0] == j)[0],:]).sum(axis=0, keepdims=True)
                    for j in list(set(centerIndices[layer][1])):
                        backpropStateForce[layer][j, :] = backpropStateForce[layer][j, :] \
                                                          - (backpropForce[np.where(centerIndices[layer][1] == j)[0],:]).sum(axis=0, keepdims=True)
                elif miscType == 'zeroFlatten':
                    backpropStateForce[layer][self.new_points_local,:] = MiscCostGradient(forwardState_[layer][self.new_points_local,:],
                                                                                          sigmaError=sigmaError_,
                                                                                          dimensions=miscLayer_[1][miscLayer_[0].index(layer)])
                else:
                    for j in range(nPatches):
                        idx = 0
                        for j_prime in patches[j]:
                            if miscType in ('repulse','flatten'):
                                i_j_o = np.copy(centerIndices[layer][j])
                                i_j_prime_o = np.copy(centerIndices[layer][j_prime])
                                backpropForce = MiscCostGradient(forwardState_[layer][[i_j_o], :],
                                                                 forwardState_[layer][[i_j_prime_o], :],
                                                                 sigmaError=sigmaError_[j][j_prime],
                                                                 dimensions=miscLayer_[1][miscLayer_[0].index(layer)],
                                                                 distance=dist)
                            elif miscType == 'center':
                                i_j_o = np.copy(centerIndices[layer][j])
                                i_j_prime_o = np.copy(centerIndices[layer][j_prime])
                                backpropForce = MiscCostGradient(ps.Cost(forwardState_[layer][[i_j_o], :],
                                                                         forwardState_[layer][[i_j_prime_o], :],
                                                                         dimensions=miscLayer_[1][miscLayer_[0].index(layer)]),
                                                                 ps.Cost(self.XTr_orig[[patch_centers[0][j]], :self.dim_X],
                                                                         self.XTr_orig[[patch_centers[0][j_prime]], :self.dim_X]),
                                                                 sigmaError=sigmaError_[j][j_prime]) \
                                                * ps.CostGradient(forwardState_[layer][[i_j_o], :],
                                                                  forwardState_[layer][[i_j_prime_o], :],
                                                                  dimensions=miscLayer_[1][miscLayer_[0].index(layer)])
                            elif miscType == 'stitch':
                                i_j_o = np.copy(self.overlapping_points[0][j][idx])
                                i_j_prime_o = np.copy(self.overlapping_points[1][j][idx])
                                backpropForce = MiscCostGradient(forwardState_[layer][i_j_o, :],
                                                                 forwardState_[layer][i_j_prime_o, :],
                                                                 sigmaError=sigmaError_[j][j_prime],
                                                                 dimensions=miscLayer_[1][miscLayer_[0].index(layer)])
                                idx += 1
                            backpropStateForce[layer][[i_j_o], :] = backpropStateForce[layer][[i_j_o],:] + backpropForce
                            backpropStateForce[layer][[i_j_prime_o], :] = backpropStateForce[layer][[i_j_prime_o],:] - backpropForce
        return backpropStateForce


    def hamiltonianGradient_LDDMM(self, px1, at, affine, kernel, regWeight, x0=None, zt=None, idx_diff=None, idx_nodiff=None, idx_clamp=None, C=1.0):
                                # called by getGradient:
                                # return dat, xt, pxt (if affine=None and getCovector=True)
                                # return dat, dA, db, xt, pxt (if affine not None and getCovector=True)
                                # i.e., includes pxt (by propagating p(1) backwards to p(0)) and dat (= p-2a)
        if x0 is None:
            if zt is not None:
                x0 = zt[0,:,:]
            else:
                logging.info('\nerror in hamiltonianGradient_LDDMM')
                exit()
        return evol.landmarkHamiltonianGradient(x0, at, px1, kernel, regWeight, getCovector=True,
                                                affine=affine, zt=zt, idx_diff=idx_diff, idx_nodiff=idx_nodiff,
                                                idx_clamp=idx_clamp, C=C)


    def hamiltonianGradient_NN(self, px1, Wt, bt, fun, weights, x0=None, zt=None):
        if x0 is None:
            if zt is not None:
                x0 = zt[0,:,:]
            else:
                logging.info('\nerror in hamiltonianGradient_NN')
                exit()
        (pxt, xt) = evol.NNCovectorEvolution(x0, Wt, bt, px1, fun=fun, zt=zt)
        dWt = np.zeros(Wt.shape)
        dbt = np.zeros(bt.shape)
        T = Wt.shape[0]
        for t in range(T):
            xtt = np.copy(xt[t, :, :])
            pxtt = np.copy(pxt[t+1, :, :])
            Wtt = np.copy(Wt[t, :, :])
            btt = np.copy(bt[t, :])

            u__ = np.dot(xtt, Wtt.T) + btt
            vt = fun().evalDiff(u__)
            vp = vt * pxtt
            dWt[t, :, :] = weights[0] * Wtt - np.dot(vp.T, xtt)
            dbt[t, :] = weights[1] * btt - vp.sum(axis=0)
        return dWt, dbt, pxt

    ####################################################################################################################
    #   gradientTools stop
    ####################################################################################################################

    ####################################################################################################################
    #   objectiveFunction start
    ####################################################################################################################

    def objectiveFun(self):  # called by cg and bfgs
                             # if self.obj == None, updates self.obj, self.zt, self.forwardState_In, and
                             #                self.forwardState_Out, and returns updated self.obj
                             # otherwise, returns current self.obj
        if self.obj == None:
            if self.vf_type == 'LDDMM':
                (self.obj, self.zt, self.forwardState_In, self.forwardState_Out) = self.objectiveFunDef_LDDMM(self.at, self.Afft, withTrajectory=True)
            elif self.vf_type == 'NN':
                (self.obj, self.zt, self.forwardState_In, self.forwardState_Out) = self.objectiveFunDef_NN(self.Wt, self.bt, withTrajectory=True)
            self.obj += self.dataTerm(self.forwardState_Out[-1])
            self.obj += self.uTerm(self.u, self.b)
            self.obj += self.miscTerms(self.forwardState_In, self.forwardState_Out)
        return self.obj


    def dataTerm(self, _finalState):    # called by objectiveFun and updateTry
                                        # calculates RegressionScore or LogisticScore
        if self.model == 'regression':
            obj = ps.RegressionScore(_finalState, self.YTr_layer[-1], sigmaError=self.sigmaError)
        elif self.model == 'classification':
            obj = ps.LogisticScore(_finalState, self.YTr_layer[-1], w=self.wTr, sigmaError=self.sigmaError)
        return obj


    def uTerm(self, _u, _b):        # called by objectiveFun and updateTry
                                    # calculates ridge regularization term
        obj = ps.RidgeRegularization(_u, _b, lam=self.lam, ridgecost=self.ridgecost)
        return obj


    def miscTerms(self, _forwardState_In, _forwardState_Out):
        obj = 0.0
        if self.repulse_flag:
            obj += self.miscTerm(_forwardState_In, self.repulseLayer_In, ps.RepulseCost, self.repulse_sigmaError_non_overlap, self.non_overlapping_patches, miscType='repulse', dist=2.0 * self.ball_eps)
            obj += self.miscTerm(_forwardState_Out, self.repulseLayer_Out, ps.RepulseCost, self.repulse_sigmaError_non_overlap, self.non_overlapping_patches, miscType='repulse', dist=2.0 * self.ball_eps)
            # obj += self.miscTerm(_forwardState_In, self.repulseLayer_In, ps.RepulseCost, self.repulse_sigmaError_overlap, self.overlapping_patches, miscType='repulse', dist=1.0 * self.ball_eps)
            # obj += self.miscTerm(_forwardState_Out, self.repulseLayer_Out, ps.RepulseCost, self.repulse_sigmaError_overlap, self.overlapping_patches, miscType='repulse', dist=1.0 * self.ball_eps)
        if self.elastic_flag:
            obj += self.miscTerm(_forwardState_In, self.elasticLayer_In, ps.ElasticCost, self.elastic_sigmaError, self.overlapping_patches_elastic, miscType='elastic')
            obj += self.miscTerm(_forwardState_Out, self.elasticLayer_Out, ps.ElasticCost, self.elastic_sigmaError, self.overlapping_patches_elastic, miscType='elastic')
        if self.elasticPatches_flag:
            obj += self.miscTerm(_forwardState_In, self.elasticPatchesLayer_In, ps.ElasticCost, self.elasticPatches_sigmaError, self.overlapping_patches, miscType='elasticPatches')
            obj += self.miscTerm(_forwardState_Out, self.elasticPatchesLayer_Out, ps.ElasticCost, self.elasticPatches_sigmaError, self.overlapping_patches, miscType='elasticPatches')
        if self.zeroFlatten_flag:
            obj += self.miscTerm(_forwardState_In, self.zeroFlattenLayer_In, ps.ZeroFlattenCost, self.zeroFlatten_sigmaError, None, miscType='zeroFlatten')
            obj += self.miscTerm(_forwardState_Out, self.zeroFlattenLayer_Out, ps.ZeroFlattenCost, self.zeroFlatten_sigmaError, None, miscType='zeroFlatten')
        if self.flatten_flag:
            obj += self.miscTerm(_forwardState_In, self.flattenLayer_In, ps.FlattenCost, self.flatten_sigmaError, self.overlapping_patches, miscType='flatten')
            obj += self.miscTerm(_forwardState_Out, self.flattenLayer_Out, ps.FlattenCost, self.flatten_sigmaError, self.overlapping_patches, miscType='flatten')
        if self.center_flag:
            obj += self.miscTerm(_forwardState_In, self.centerLayer_In, ps.Cost, self.center_sigmaError, self.overlapping_patches, miscType='center')
            obj += self.miscTerm(_forwardState_Out, self.centerLayer_Out, ps.Cost, self.center_sigmaError, self.overlapping_patches, miscType='center')
        if self.stitchPatches_flag:
            obj += self.miscTerm(_forwardState_In, self.stitchLayer_In, ps.Cost, self.stitch_sigmaError, self.overlapping_patches, miscType='stitch')
            obj += self.miscTerm(_forwardState_Out, self.stitchLayer_Out, ps.Cost, self.stitch_sigmaError, self.overlapping_patches, miscType='stitch')
        return obj


    def miscTerm(self, forwardState_, miscLayer_, MiscCost, sigmaError_, patches, miscType='repulse', dist=1.0):
        obj = 0.0
        if miscType == 'elastic':
            centerIndices = self.centerIndices_elastic
            XTr_centerIndices = self.XTr_centerIndices_elastic
        elif miscType == 'elasticPatches':
            centerIndices = self.centerIndices_elasticPatches
            XTr_centerIndices = self.XTr_centerIndices_elasticPatches
        else:
            nPatches = self.nPatches
            centerIndices = self.centerIndices
            patch_centers = self.patch_centers
        for layer in range(self.numLayers):
            if layer in miscLayer_[0]:
                if miscType in ('elastic','elasticPatches'):
                    obj += MiscCost(forwardState_[layer][centerIndices[layer][0], :],
                                    forwardState_[layer][centerIndices[layer][1], :],
                                    self.XTr_orig[XTr_centerIndices[0], :self.dim_X],
                                    self.XTr_orig[XTr_centerIndices[1], :self.dim_X],
                                    sigmaError=sigmaError_,
                                    dimensions=miscLayer_[1][miscLayer_[0].index(layer)])
                elif miscType == 'zeroFlatten':
                    obj += MiscCost(forwardState_[layer][self.new_points_local,:], sigmaError=sigmaError_, dimensions=miscLayer_[1][miscLayer_[0].index(layer)])
                else:
                    for j in range(nPatches):
                        idx = 0
                        for j_prime in patches[j]:  # j_prime > j in overlapping_patches[j] or non_overlapping_patches[j]
                            if miscType in ('repulse', 'flatten'):
                                i_j_o = np.copy(centerIndices[layer][j])
                                i_j_prime_o = np.copy(centerIndices[layer][j_prime])
                                obj += MiscCost(forwardState_[layer][[i_j_o], :],
                                                forwardState_[layer][[i_j_prime_o], :],
                                                sigmaError=sigmaError_[j][j_prime],
                                                dimensions=miscLayer_[1][miscLayer_[0].index(layer)],
                                                distance=dist)
                            elif miscType == 'center':
                                i_j_o = np.copy(centerIndices[layer][j])
                                i_j_prime_o = np.copy(centerIndices[layer][j_prime])
                                obj += MiscCost(ps.Cost(forwardState_[layer][[i_j_o], :],
                                                        forwardState_[layer][[i_j_prime_o], :],
                                                        dimensions=miscLayer_[1][miscLayer_[0].index(layer)]),
                                                ps.Cost(self.XTr_orig[[patch_centers[0][j]], :self.dim_X],
                                                        self.XTr_orig[[patch_centers[0][j_prime]], :self.dim_X]),
                                                sigmaError=sigmaError_[j][j_prime])
                            elif miscType == 'stitch':
                                i_j_o = np.copy(self.overlapping_points[0][j][idx])
                                i_j_prime_o = np.copy(self.overlapping_points[1][j][idx])
                                obj += MiscCost(forwardState_[layer][i_j_o, :],
                                               forwardState_[layer][i_j_prime_o, :],
                                               sigmaError=sigmaError_[j][j_prime],
                                               dimensions=miscLayer_[1][miscLayer_[0].index(layer)])
                                idx += 1
        return obj


    def objectiveFunDef_LDDMM(self, at, Afft, kernel=None, withTrajectory=False, withJacobian=False,
                              x0=None, u=None, b=None, gam=None):
                            # called by objectiveFun, updateTry, fit, and endOfIteration
                            # calculates and returns obj_LDDMM,
                            #                        zt, forwardStateIn, forwardStateOut (if withTrajectory=True),
                            #                    and Jt (if withJacobian=True)
        if gam is None:
            gam = self.gam
        if x0 is None:
            x0 = self.XTr
        if kernel is None:
            kernel = self.KparDiff
        if u is None:
            u = self.u
        if b is None:
            b = self.b
        zt = [np.zeros((self.Tsize[layer_D] + 1, self.NTr_layer[self.layer_D_indices[layer_D]], self.dim_D[layer_D])) for layer_D in range(self.nD)]
        Jt = [np.zeros((self.Tsize[layer_D] + 1, self.N_controls[layer_D], 1)) for layer_D in range(self.nD)]
        forwardStateIn = [None] * self.numLayers
        forwardStateOut = [None] * self.numLayers
        for layer_D in range(self.nD):
            forwardStateIn[self.layer_D_indices[layer_D]] = np.zeros((self.NTr_layer[self.layer_D_indices[layer_D]], self.dim_D[layer_D]))
            forwardStateOut[self.layer_D_indices[layer_D]] = np.zeros((self.NTr_layer[self.layer_D_indices[layer_D]], self.dim_D[layer_D]))
        obj = 0.0
        A = self.getAffine(Afft=Afft)
        zTr = np.copy(x0)
        for layer in range(self.numLayers):
            if layer in self.removeDimLayer_In[0]:
                zTr = np.delete(zTr, self.removeDimLayer_In[1][self.removeDimLayer_In[0].index(layer)], axis=1)
            if layer in self.addDimLayer_In[0]:
                zTr = ps.insertZeroColumns(zTr, self.addDimLayer_In[1][self.addDimLayer_In[0].index(layer)])
            if self.createPatches[layer]:
                zTr_all_patches = np.copy(zTr[self.points_in_patch[0], :])
                for patch in range(1, self.nPatches):
                    zTr_all_patches = np.concatenate((zTr_all_patches, zTr[self.points_in_patch[patch], :]), axis=0)
                zTr = np.copy(zTr_all_patches)
            forwardStateIn[layer] = np.copy(zTr)
            if self.layers[layer] == 'D':
                layer_D = self.layer_D_indices.index(layer)
                for patch in range(len(self.idx_diff[layer_D])):
                    withPointSet = None
                    if self.trainSubset_flag[layer_D]: withPointSet = zTr[self.idx_nodiff[layer_D][patch], :]
                    trainRes = evol.landmarkDirectEvolutionEuler(zTr[self.idx_diff[layer_D][patch], :], at[layer_D][:,self.idx_controls[layer_D][patch],:],
                                                                 kernel[layer_D], affine=A[layer_D][patch], withJacobian=withJacobian, withPointSet=withPointSet)
                    zt[layer_D][:, self.idx_diff[layer_D][patch], :] = np.copy(trainRes[0])
                    if self.trainSubset_flag[layer_D]: zt[layer_D][:, self.idx_nodiff[layer_D][patch], :] = np.copy(trainRes[1])
                    if withJacobian: Jt[layer_D][:,self.idx_controls[layer_D][patch],:] = np.copy(trainRes[-1])
                    obj1 = 0.0
                    for t in range(self.Tsize[layer_D]):
                        z_ = zt[layer_D][t, self.idx_diff[layer_D][patch], :]
                        a = at[layer_D][t, self.idx_controls[layer_D][patch], :]
                        ra = kernel[layer_D].applyK(z_, a)
                        if hasattr(self, 'v'): self.v[layer_D][t, self.idx_controls[layer_D][patch], :] = ra
                        obj = obj + gam[layer_D] * self.timeStep[layer_D] * np.multiply(a, (ra)).sum()
                        if self.affineDim[layer_D] > 0:
                            obj1 += self.timeStep[layer_D] \
                                    * np.multiply(self.affineWeight[layer_D].reshape(Afft[layer_D][patch,t,:].shape),
                                                  Afft[layer_D][patch,t,:] ** 2).sum()
                    obj += obj1
                    if layer_D in self.clamp:
                        if self.idx_clamp[layer_D] is not None:
                            obj2 = 0.0
                            for t in range(self.Tsize[layer_D]):
                                z_ = zt[layer_D][t, self.idx_diff[layer_D][patch], :]
                                a = at[layer_D][t, self.idx_controls[layer_D][patch], :]
                                ra = kernel[layer_D].applyK(z_, a, firstVar=zt[layer_D][t, self.idx_clamp[layer_D], :])
                                obj2 = obj2 + self.C * self.timeStep[layer_D] * np.multiply((ra), (ra)).sum()
                            obj += obj2
                zTr = np.copy(zt[layer_D][-1, :, :])
            else:
                layer_A = self.layer_A_indices.index(layer)
                zTr = self.affineMult[layer_A](zTr, u[layer_A], b[layer_A])
            forwardStateOut[layer] = np.copy(zTr)

        if withJacobian:
            return obj, zt, forwardStateIn, forwardStateOut, Jt
        elif withTrajectory:
            return obj, zt, forwardStateIn, forwardStateOut
        else:
            return obj


    def getAffine(self, Afft=None):
        A = [[None for patch in range(self.nPatches)] if self.layer_D_indices[layer_D] in self.patchesLayer else [None] for layer_D in range(self.nD)]
        if Afft is None:
            Afft_ = copy.deepcopy(self.Afft)
        else:
            Afft_ = copy.deepcopy(Afft)
        for layer_D in range(self.nD):
            for patch in range(Afft_[layer_D].shape[0]):
                if self.affineDim[layer_D] > 0:
                    A[layer_D][patch] = self.affB[layer_D].getTransforms(Afft_[layer_D][patch, :, :])
        return A


    def objectiveFunDef_NN(self, Wt, bt, withTrajectory=False, x0=None, u=None, b=None, weights=None):
        if weights is None:
            weights = self.weights
        if x0 is None:
            x0 = self.XTr
        if u is None:
            u = self.u
        if b is None:
            b = self.b
        zt = [None] * self.nD
        for layer_D in range(self.nD):
            zt[layer_D] = np.zeros((self.Tsize[layer_D] + 1, self.NTr_layer[self.layer_D_indices[layer_D]], self.dim_D[layer_D]))
        forwardStateIn = [None] * self.numLayers
        forwardStateOut = [None] * self.numLayers
        obj = 0
        zTr = np.copy(x0)
        for layer in range(self.numLayers):
            forwardStateIn[layer] = np.copy(zTr)
            if self.layers[layer] == 'D':
                layer_D = self.layer_D_indices.index(layer)
                zt[layer_D] = evol.NNEvolution(zTr, Wt[layer_D], bt[layer_D], fun=self.fun[layer_D])
                zTr = np.copy(zt[layer_D][-1, :, :])
                obj += (weights[layer_D][0] * (Wt[layer_D] ** 2).sum() + weights[layer_D][1] * (bt[layer_D] ** 2).sum()) \
                       / (2 * self.Tsize[layer_D])
            else:
                layer_A = self.layer_A_indices.index(layer)
                zTr = self.affineMult[layer_A](zTr, u[layer_A], b[layer_A])
            forwardStateOut[layer] = np.copy(zTr)
        if withTrajectory:
            return obj, zt, forwardStateIn, forwardStateOut
        else:
            return obj

    ####################################################################################################################
    #   objectiveFunction stop
    ####################################################################################################################

    ####################################################################################################################
    #   optimizationTools start
    ####################################################################################################################

    def getVariable(self):
        if self.vf_type == 'LDDMM':
            return [self.at, self.Afft, self.u]
        elif self.vf_type == 'NN':
            return [self.Wt, self.bt, self.u]


    def acceptVarTry(self):
        if self.vf_type == 'LDDMM':
            self.at = copy.deepcopy(self.atTry)
            self.Afft = copy.deepcopy(self.AfftTry)
        elif self.vf_type == 'NN':
            self.Wt = copy.deepcopy(self.WtTry)
            self.bt = copy.deepcopy(self.btTry)
        self.obj = np.copy(self.objTry)
        self.u = copy.deepcopy(self.uTry)
        self.b = copy.deepcopy(self.bTry)
        self.zt = copy.deepcopy(self.ztTry)                             # zt, forwardState_In, and forwardState_Out
        self.forwardState_In = copy.deepcopy(self.forwardStateInTry)    #   are NOT variables; however, updating them
        self.forwardState_Out = copy.deepcopy(self.forwardStateOutTry)  #   here bypasses unnecessary propagation later


    def updateTry(self, dir, eps, objRef=None, log=True):
                            # called by cg and bfgs
                            # takes a step in direction dir (grd) with step size eps (learning rate)
                            # and calculates atTry, AfftTry, objTry, uTry, and bTry  (for self.vf_type == 'LDDMM')
                            #             or WtTry, btTry, objTry, uTry, and bTry    (for self.vf_type == 'NN')
                            # returns objTry
                            # saves [uTry, bTry, ztTry, forwardStateInTry, forwardStateOutTry, atTry, AfftTry, dir, eps]
                            #    or [uTry, bTry, ztTry, forwardStateInTry, forwardStateOutTry, WtTry, btTry, dir, eps]
                            #   in self.updated_lineSearch for use in bfgs linesearch
                            # if objTry < objRef or objRef == None, updates
                            #        self.atTry, self.AfftTry, self.objTry, self.uTry, self.bTry, self.ztTry, self.forwardStateInTry, self.forwardStateOutTry
                            #     or self.WtTry, self.btTry, self.objTry, self.uTry, self.bTry, self.ztTry, self.forwardStateInTry, self.forwardStateOutTry
                            # (note:  ztTry, forwardStateInTry, and forwardStateOutTry are NOT variables;
                            #         however, updating them here bypasses unnecessary propagation later)
        objTry = 0
        uTry = [(self.u[layer_A] - eps * dir.upred[layer_A]) for layer_A in range(self.nA)]
        bTry = [(self.b[layer_A] - eps * dir.bpred[layer_A]) for layer_A in range(self.nA)]
        if self.vf_type == 'LDDMM':
            atTry = [(self.at[layer_D] - eps * dir.diff[layer_D]) for layer_D in range(self.nD)]
            AfftTry = copy.deepcopy(self.Afft)
            AfftTry = [(self.Afft[layer_D] - eps * dir.aff[layer_D]) if self.affineDim[layer_D] > 0 else AfftTry[layer_D] for layer_D in range(self.nD)]
            foo = self.objectiveFunDef_LDDMM(atTry, AfftTry, u=uTry, b=bTry, withTrajectory=True)   # propagate each zt[layer_D] trajectory forward,
                                                                                            # then return obj_LDDMM, trajectories, and forward states
        elif self.vf_type == 'NN':
            WtTry = [(self.Wt[layer_D] - eps * dir.W[layer_D]) for layer_D in range(self.nD)]
            btTry = [(self.bt[layer_D] - eps * dir.b[layer_D]) for layer_D in range(self.nD)]
            foo = self.objectiveFunDef_NN(WtTry, btTry, u=uTry, b=bTry, withTrajectory=True)    # propagate each zt[layer_D] trajectory forward,
                                                                                        # then return obj_NN, trajectories, and forward states
        objTry += foo[0]  # add obj_LDDMM or obj_NN to objTry
        forwardStateInTry = copy.deepcopy(foo[2])
        forwardStateOutTry = copy.deepcopy(foo[3])
        finalStateTry = forwardStateOutTry[-1]
        dataTermTry = self.dataTerm(finalStateTry)
        uTermTry = self.uTerm(uTry, bTry)
        miscTermsTry = self.miscTerms(forwardStateInTry, forwardStateOutTry)
        objTry += dataTermTry   # add dataTerm (using finalStateTry) to objTry
        objTry += uTermTry      # add uTerm (using uTry) to objTry
        objTry += miscTermsTry
        if log:
            logging.info(' '*100 + '{0:.4f}  {1:.4f}  {2:.4f}  {3:.4f}'.format(foo[0], dataTermTry, uTermTry, miscTermsTry))
        ztTry = copy.deepcopy(foo[1])
        if self.vf_type == 'LDDMM':
            self.updated_lineSearch = [uTry, bTry, ztTry, forwardStateInTry, forwardStateOutTry, atTry, AfftTry, dir, eps]
        elif self.vf_type == 'NN':
            self.updated_lineSearch = [uTry, bTry, ztTry, forwardStateInTry, forwardStateOutTry, WtTry, btTry, dir, eps]
        if np.isnan(objTry):
            logging.info('Warning: nan in updateTry')
            return 1e500
        if (objRef == None) or (objTry < objRef):
            if self.vf_type == 'LDDMM':
                self.atTry = copy.deepcopy(atTry)
                self.AfftTry = copy.deepcopy(AfftTry)
            elif self.vf_type == 'NN':
                self.WtTry = copy.deepcopy(WtTry)
                self.btTry = copy.deepcopy(btTry)
            self.objTry = np.copy(objTry)
            self.uTry = copy.deepcopy(uTry)
            self.bTry = copy.deepcopy(bTry)
            self.ztTry = copy.deepcopy(ztTry)                               # not a variable
            self.forwardStateInTry = copy.deepcopy(forwardStateInTry)       # not a variable
            self.forwardStateOutTry = copy.deepcopy(forwardStateOutTry)     # not a variable
        return objTry


    def addProd(self, dir1, dir2, beta):
        dir = Direction(vf_type=self.vf_type, nD=self.nD, nA=self.nA)
        if self.vf_type == 'LDDMM':
            dir.diff = [(dir1.diff[layer_D] + beta * dir2.diff[layer_D]) for layer_D in range(self.nD)]
            dir.aff = [(dir1.aff[layer_D] + beta * dir2.aff[layer_D]) for layer_D in range(self.nD)]
        elif self.vf_type == 'NN':
            dir.W = [(dir1.W[layer_D] + beta * dir2.W[layer_D]) for layer_D in range(self.nD)]
            dir.b = [(dir1.b[layer_D] + beta * dir2.b[layer_D]) for layer_D in range(self.nD)]
        dir.upred = [(dir1.upred[layer_A] + beta * dir2.upred[layer_A]) for layer_A in range(self.nA)]
        dir.bpred = [(dir1.bpred[layer_A] + beta * dir2.bpred[layer_A]) for layer_A in range(self.nA)]
        return dir


    def prod(self, dir1, beta):
        dir = Direction(vf_type=self.vf_type, nD=self.nD, nA=self.nA)
        if self.vf_type == 'LDDMM':
            dir.diff = [beta * dir1.diff[layer_D] for layer_D in range(self.nD)]
            dir.aff = [beta * dir1.aff[layer_D] for layer_D in range(self.nD)]
        elif self.vf_type == 'NN':
            dir.W = [beta * dir1.W[layer_D] for layer_D in range(self.nD)]
            dir.b = [beta * dir1.b[layer_D] for layer_D in range(self.nD)]
        dir.upred = [beta * dir1.upred[layer_A] for layer_A in range(self.nA)]
        dir.bpred = [beta * dir1.bpred[layer_A] for layer_A in range(self.nA)]
        return dir


    def copyDir(self, dir0):
        dir = Direction(vf_type=self.vf_type, nD=self.nD, nA=self.nA)
        if self.vf_type == 'LDDMM':
            dir.diff = copy.deepcopy(dir0.diff)
            dir.aff = copy.deepcopy(dir0.aff)
        elif self.vf_type == 'NN':
            dir.W = copy.deepcopy(dir0.W)
            dir.b = copy.deepcopy(dir0.b)
        dir.upred = copy.deepcopy(dir0.upred)
        dir.bpred = copy.deepcopy(dir0.bpred)
        return dir


    def randomDir(self):
        dirfoo = Direction(vf_type=self.vf_type, nD=self.nD, nA=self.nA)
        if self.vf_type == 'LDDMM':
            for layer_D in range(self.nD):
                if not self.freezeLayer[self.layer_D_indices[layer_D]]:
                    dirfoo.diff[layer_D] = np.random.normal(size=self.at[layer_D].shape)
                    dirfoo.aff[layer_D] = np.random.normal(size=self.Afft[layer_D].shape)
                else:
                    dirfoo.diff[layer_D] = np.zeros(self.at[layer_D].shape)
                    dirfoo.aff[layer_D] = np.zeros(self.Afft[layer_D].shape)
        elif self.vf_type == 'NN':
            for layer_D in range(self.nD):
                dirfoo.W[layer_D] = np.random.normal(size=self.Wt[layer_D].shape)
                dirfoo.b[layer_D] = np.random.normal(size=self.bt[layer_D].shape)
        for layer_A in range(self.nA):
            if not self.freezeLayer[self.layer_A_indices[layer_A]]:
                dirfoo.upred[layer_A] = np.random.normal(size=self.u[layer_A].shape)
                if self.b_flag[layer_A]:
                    dirfoo.bpred[layer_A] = np.random.normal(size=self.b[layer_A].shape)
                else:
                    dirfoo.bpred[layer_A] = np.zeros(self.b[layer_A].shape)
            else:
                dirfoo.upred[layer_A] = np.zeros(self.u[layer_A].shape)
                dirfoo.bpred[layer_A] = np.zeros(self.b[layer_A].shape)
        if self.model == 'classification':
            dirfoo.upred[-1][:, 0] = 0
            dirfoo.bpred[-1][0, 0] = 0
        return dirfoo


    def dotProduct(self, g1, g2):   # used in cg
        if self.vf_type == 'LDDMM':
            res = np.zeros(len(g2))
            for layer_D in range(self.nD):
                for patch in range(len(self.idx_diff[layer_D])):
                    for t in range(self.Tsize[layer_D]):
                        x = self.zt[layer_D][t, self.idx_diff[layer_D][patch], :]
                        gg = g1.diff[layer_D][t, self.idx_controls[layer_D][patch], :]
                        u = self.KparDiff[layer_D].applyK(x, gg)
                        uu = g1.aff[layer_D][patch, t, :]
                        ll = 0
                        for gr in g2:
                            ggOld = gr.diff[layer_D][t, self.idx_controls[layer_D][patch], :]
                            res[ll] = res[ll] + np.multiply(ggOld, u).sum()
                            if self.affineDim[layer_D] > 0:
                                res[ll] += np.multiply(uu, gr.aff[layer_D][patch, t, :]).sum() * self.coeffAff
                            ll = ll + 1
            pp = copy.deepcopy(g1.upred)
            pb = copy.deepcopy(g1.bpred)
            for layer_A in range(self.nA):
                ll = 0
                for gr in g2:
                    res[ll] += (np.concatenate((pb[layer_A].ravel(), pp[layer_A].ravel()), axis=0)
                                * np.concatenate((gr.bpred[layer_A].ravel(), gr.upred[layer_A].ravel()), axis=0)).sum() * self.coeffuPred
                    ll += 1
            return res
        elif self.vf_type == 'NN':
            return self.dotProduct_euclidean(g1, g2)


    def dotProduct_euclidean(self, g1, g2): # used in bfgs
                                            # called by dotProduct for self.vf_type == 'NN'
        res = np.zeros(len(g2))
        if self.vf_type == 'LDDMM':
            for layer_D in range(self.nD):
                for t in range(self.Tsize[layer_D]):
                    gg = g1.diff[layer_D][t, :, :]
                    uu = g1.aff[layer_D][:, t, :]
                    ll = 0
                    for gr in g2:
                        ggOld = gr.diff[layer_D][t, :, :]
                        res[ll] = res[ll] + np.multiply(ggOld, gg).sum()
                        if self.affineDim[layer_D] > 0:
                            res[ll] += np.multiply(uu, gr.aff[layer_D][:, t, :]).sum() * self.coeffAff
                        ll = ll + 1
        elif self.vf_type == 'NN':
            for layer_D in range(self.nD):
                for ll, gr in enumerate(g2):
                    res[ll] += (gr.W[layer_D] * g1.W[layer_D]).sum() + (gr.b[layer_D] * g1.b[layer_D]).sum()
        pp = copy.deepcopy(g1.upred)
        pb = copy.deepcopy(g1.bpred)
        for layer_A in range(self.nA):
            ll = 0
            for gr in g2:
                res[ll] += (np.concatenate((pb[layer_A].ravel(), pp[layer_A].ravel()), axis=0)
                            * np.concatenate((gr.bpred[layer_A].ravel(), gr.upred[layer_A].ravel()), axis=0)).sum() * self.coeffuPred
                ll += 1
        return res


    def stopCondition(self):  # called by bfgs
        return False


    def startOfIteration(self):
        if self.reset:
            if self.vf_type == 'LDDMM':
                for K in self.KparDiff:
                    K.dtype = 'float64'


    def endOfProcedure(self):  # called by cg and bfgs
        self.endOfIteration(endP=True)

    ####################################################################################################################
    #   optimizationTools stop
    ####################################################################################################################

    ####################################################################################################################
    #   endOfIteration start
    ####################################################################################################################

    def endOfIteration(self, endP=False):  # called by cg and bfgs, and endOfProcedure
        if not endP:
            self.iter += 1

        if self.iter >= self.affBurnIn:
            self.coeffAff = self.coeffAff2

        if (self.iter % self.saveRate == 0 or endP):
            if endP:
                logging.info('-------------------------------endP-------------------------------')
                if self.vf_type == 'LDDMM':
                    self.obj, self.zt, self.forwardState_In, self.forwardState_Out = self.objectiveFunDef_LDDMM(self.at,
                                                                                                                self.Afft,
                                                                                                                withTrajectory=True)
                elif self.vf_type == 'NN':
                    self.obj, self.zt, self.forwardState_In, self.forwardState_Out = self.objectiveFunDef_NN(self.Wt,
                                                                                                             self.bt,
                                                                                                             withTrajectory=True)
            if self.model == 'regression':
                self.guTr = np.copy(self.forwardState_Out[-1][:,:self.dim_Y])
            elif self.model == 'classification':
                self.guTr = np.argmax(self.forwardState_Out[-1], axis=1)[:, np.newaxis]
            self.calculateErrors()
            self.concatenateResults()
            self.running_plot()

        if self.vf_type == 'LDDMM':
            for k,K in enumerate(self.KparDiff):
                K.dtype = self.Kdtype[k]

    ####################################################################################################################
    #   endOfIteration stop
    ####################################################################################################################

    ####################################################################################################################
    #   calculateErrors and concatenateResults start
    ####################################################################################################################

    def calculateErrors(self):
        if self.model == 'regression':
            if self.standardize_fit_y:
                self.train_err = (np.multiply(self.guTr - self.YTr_layer[-1], self.s0_y) ** 2).sum() / self.NTr_layer[-1]
                self.train_results = np.multiply(self.guTr, self.s0_y) + self.mu0_y
            else:
                self.train_err = ((self.guTr - self.YTr_layer[-1]) ** 2).sum() / self.NTr_layer[-1]
                self.train_results = np.copy(self.guTr)
        elif self.model == 'classification':
            self.train_err = np.sum(np.not_equal(self.guTr, self.YTr_layer[-1]) * self.wTr) / self.swTr
            self.train_results = np.copy(self.guTr)
        logging.info('Training Error {0:.8f}'.format(np.squeeze(self.train_err)))
        for layer_A in range(self.nA):
            U, self.u_sVals[layer_A], V = np.linalg.svd(self.u[layer_A])


    def concatenateResults(self):
        if len(self.trainError_array) == 0:
            self.trainError_array = np.copy(self.train_err[np.newaxis,np.newaxis])
            for layer_A in range(self.nA):
                self.u_sVals_array[layer_A] = np.copy(self.u_sVals[layer_A][:,np.newaxis])
            self.obj_array = np.copy(self.obj[np.newaxis,np.newaxis])
        else:
            self.trainError_array = np.concatenate((self.trainError_array, self.train_err[np.newaxis,np.newaxis]), axis=1)
            for layer_A in range(self.nA):
                self.u_sVals_array[layer_A] = np.concatenate((self.u_sVals_array[layer_A], self.u_sVals[layer_A][:,np.newaxis]), axis=1)
            self.obj_array = np.concatenate((self.obj_array, self.obj[np.newaxis,np.newaxis]), axis=1)

    ####################################################################################################################
    #   calculateErrors and concatenateResults stop
    ####################################################################################################################
