# Diffeomorphic Learning

The main function is diffeoLearning.py

This code uses the following python packages:
numpy~=1.19.2
scipy~=1.4.1
mnist~=0.2.2
pandas~=1.1.3
scikit-learn~=0.23.2
pillow~=8.0.1
matplotlib~=3.3.2
numba~=0.51.2
pylatex~=1.4.1