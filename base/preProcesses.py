import logging
import copy
import numpy as np
from numpy.linalg import solve
from scipy.stats import mode
from sklearn.neighbors import NearestNeighbors
from sklearn.metrics import pairwise_distances
import matplotlib
import matplotlib.pyplot as plt
from base.pointSets import epsilonNet


def standardizeData(x, standardize='x', model='regression'):
    if standardize == 'x':
        mu0_x = np.mean(x, axis=0, keepdims=True)
        s0_x = np.std(x, axis=0, keepdims=True)
        s0_x[:, np.argwhere(s0_x == 0.0)[:, 1]] = 1.0
        x = np.divide(x - mu0_x, s0_x)
        return x, mu0_x, s0_x
    elif standardize == 'y':
        y = np.copy(x)
        if model == 'regression':
            mu0_y = np.mean(y, axis=0, keepdims=True)
            s0_y = np.std(y, axis=0, keepdims=True)
            s0_y[:, np.argwhere(s0_y == 0.0)[:, 1]] = 1.0
            y = np.divide(y - mu0_y, s0_y)
            return y, mu0_y, s0_y
        elif model == 'classification':  # calculate nClasses, wTr, and swTr
            nClasses = y.max() + 1
            nTr = np.array([(y == k).sum() for k in range(nClasses)])
            wTr = float(y.size) / (nTr[y[:, 0]] * nClasses)[:, np.newaxis]
            swTr = wTr.sum()
            return nClasses, wTr, swTr


def assignDimensions(layers, layerOutDim, dim_X, dim_Y, nD, nA, addDim_X, addDimLayer_In, removeDimLayer_In,
                     addDim_predictedY, addDim_Y, ridgeType):
    logging.info('-' * 121)
    logging.info('addDim_X = {0:d}'.format(addDim_X))
    logging.info('addDim_predictedY = {0:d}'.format(addDim_predictedY))
    logging.info('addDim_Y = {0:d}'.format(addDim_Y))
    # assign dimensions to all layers
    numLayers = len(layers)
    if len(layerOutDim) == 1 and layerOutDim[0] is None:
        layerOutDim = layerOutDim * numLayers
    if len(layerOutDim) != numLayers:
        logging.info('\nlength of layerOutDim not equal to length of layers')
        exit()
    if 0 in addDimLayer_In[0]:
        logging.info('\naddDimLayer cannot contain layer 0')
        exit()
    if 0 in removeDimLayer_In[0]:
        logging.info('\nremoveDimLayer cannot contain layer 0')
        exit()
    layer_A_indices = [i for i, val in enumerate(layers) if val == 'A']
    layer_D_indices = [i for i, val in enumerate(layers) if val == 'D']
    dim_A = [None] * nA
    dim_D = [None] * nD
    dim_all_layers = [None] * len(layers)
    ind_D = 0
    ind_A = 0
    # fix last layer(s) based on dim_Y, addDim_Y, and addDim_predictedY
    layerOutDim[-1] = (dim_Y+addDim_Y) + addDim_predictedY
    for index in range(numLayers-1,-1,-1):
        if layers[index] == 'D':
            if layerOutDim[index-1] is None:
                layerOutDim[index-1] = layerOutDim[index]

                for i in range(len(addDimLayer_In[0])):
                    if addDimLayer_In[0][i] == index:
                        layerOutDim[index-1] -= int(len(addDimLayer_In[1][i]))

                for i in range(len(removeDimLayer_In[0])):
                    if removeDimLayer_In[0][i] == index:
                        layerOutDim[index-1] += int(len(removeDimLayer_In[1][i]))

        else:
            break
    # go back to first layer and start assigning dimensions
    for index in range(numLayers):
        remove_from_layer = 0  # remove dimensions at beginning of a layer
        for i in range(len(removeDimLayer_In[0])):
            if removeDimLayer_In[0][i] == index:
                remove_from_layer = int(len(removeDimLayer_In[1][i]))
        add_to_layer = 0  # add dimensions to beginning of a layer
        for i in range(len(addDimLayer_In[0])):
            if addDimLayer_In[0][i] == index:
                add_to_layer = int(len(addDimLayer_In[1][i]))
        if index == 0:
            inputDim = dim_X
            inputDim += addDim_X
        else:
            inputDim = layerOutDim[index-1]
            inputDim -= remove_from_layer
            inputDim += add_to_layer
        if layerOutDim[index] is None:
            if layers[index+1] == 'D' and layerOutDim[index+1] is not None:
                outputDim = layerOutDim[index+1]
            else:
                outputDim = inputDim
            layerOutDim[index] = outputDim
        else:
            outputDim = layerOutDim[index]
        if layers[index] == 'D':
            if inputDim == outputDim:
                dim_all_layers[index] = inputDim
                dim_D[ind_D] = inputDim
                ind_D += 1
            else:
                logging.info('\nlayer {0} is a D layer with unmatching input ({1}) and output ({2}) dimensions'.format(index,inputDim,outputDim))
                exit()
        elif layers[index] == 'A':
            dim_all_layers[index] = (inputDim + 1, outputDim)
            dim_A[ind_A] = (inputDim + 1, outputDim)
            ind_A += 1
    # check ridgeType for each A layer
    if len(ridgeType) != nA:
        logging.info('\nridgeType should have {0} entries'.format(nA))
        exit()
    if layers[-1] == 'A' and ridgeType[-1] != 'reg':
        logging.info('setting last A to ridgeType reg')
        ridgeType[-1] = 'reg'
    for layer_A in range(nA):
        if ridgeType[layer_A] != 'reg' and (dim_A[layer_A][0] - 1 != dim_A[layer_A][1]):
            logging.info('layer_A {0} not square; setting to ridgeType reg')
            ridgeType[layer_A] = 'reg'
    logging.info('updated layerOutDim = {0}'.format(layerOutDim))
    if nA > 0: logging.info('updated ridgeType = {0}'.format(ridgeType))
    logging.info('dim_all_layers = {0}'.format(dim_all_layers))
    logging.info('dim_D = {0}'.format(dim_D))
    logging.info('-' * 121)
    return layerOutDim, dim_D, dim_A, dim_all_layers, layer_D_indices, layer_A_indices, ridgeType


def estimate_training_threshold(XTr, YTr, s0_y, prediction = 'regression'):
    d = XTr.shape[1]
    nn = min(2*d+1, XTr.shape[0]//5)
    nbrs = NearestNeighbors(n_neighbors=nn, metric='euclidean').fit(XTr)
    distx, I = nbrs.kneighbors(XTr)
    if prediction == 'regression':
        disty = np.zeros(YTr.shape[1])
        distg = 0

        grd = np.zeros((YTr.shape[0], YTr.shape[1], XTr.shape[1]))
        for i in range(XTr.shape[0]):
            X = XTr[I[i, 1:], :] - XTr[I[i, 0], :]
            Y = YTr[I[i, 1:], :] - YTr[I[i, 0], :]
            cov = 0.01*np.eye(d) + np.dot(X.T, X)
            covy = np.dot(X.T, Y)
            grd[i, :, :] = solve(cov, covy).T
            res = ((Y.T - np.dot(grd[i,:,:], X.T))**2).mean()  # Y --> Y.T
            distg += res

        distg /= XTr.shape[0]

        for k in range(YTr.shape[1]):
            u = ((YTr[I[:, 1], k] - YTr[I[:, 0], k] - (grd[:, k, :] * (XTr[I[:, 1], :] - XTr[I[:, 0], :])).sum(axis=1)) ** 2).mean()
            disty[k] += u * s0_y[0, k] ** 2   # s0_y[k, 0] ** 2  --> s0_y[0, k] ** 2

        return disty/2
    elif prediction == 'classification':
        J = np.argsort(distx[:, 1])[:20]
        err1 = (YTr[I[J, 1], :] != YTr[I[J, 0], :]).mean()
        logging.info(f'error 1: {err1:.4f}')
        md = mode(YTr[I[:,1:6], 0], axis=1)
        J = md.count == md.count.max()
        pred = md.mode[J]
        err = (YTr[J] != pred).mean()
        return min(err, err1)


def calcPatches(x, labels=None, ball_eps=None, rate=None, num_neighbors=None, small_dataset=False,
                patch_on_every_point=False, relabel_flag=False, consolidate_patches=False, no_overlap=False):
    #rate = 0.05  # 0.01 decreasing rate increases epsilon and decreases number of points
    if patch_on_every_point:
        relabel_flag = False
        centers = np.arange(x.shape[0])
        eps = ball_eps
    else:
        centers, idx, eps = epsilonNet(x, ball_eps=ball_eps, rate=rate)  # , num_neighbors=num_neighbors, small_dataset=small_dataset)

    n_patches = len(centers)
    dim = x.shape[1]
    if small_dataset:
        n_samples = x.shape[0]
        dist2 = ((x.reshape([n_samples, 1, dim]) -
                  x[centers, :].reshape([1, n_patches, dim])) ** 2).sum(axis=2)
        points_in_each_patch = [np.nonzero(dist2[:, index] < (eps) ** 2)[0] for index in range(n_patches)]
    else:
        points_in_each_patch = [np.nonzero(pairwise_distances(x,x[[centers[index]], :], metric='sqeuclidean') < (eps) ** 2)[0]
                                for index in range(n_patches)]

    points_in_each_patch_orig = points_in_each_patch.copy()
    if relabel_flag:
        if consolidate_patches:
            new_unconsolidated_patch_array, points_in_each_consolidated_patch = relabel_patches(points_in_each_patch,
                                                                                                consolidate_patches=True,
                                                                                                no_overlap=no_overlap)
            points_in_each_patch_orig = [points_in_each_patch[new_unconsolidated_patch_array[idx]] for idx in range(n_patches)]
            points_in_each_patch = points_in_each_consolidated_patch.copy()
        else:
            new_unconsolidated_patch_array = relabel_patches(points_in_each_patch, no_overlap=no_overlap)
            points_in_each_patch = [points_in_each_patch[new_unconsolidated_patch_array[idx]] for idx in range(n_patches)]
            points_in_each_patch_orig = points_in_each_patch.copy()
        centers = np.array([centers[new_unconsolidated_patch_array[idx]] for idx in range(n_patches)])
        if small_dataset:
            dist2 = ((x.reshape([n_samples, 1, dim]) -
                      x[centers, :].reshape([1, n_patches, dim])) ** 2).sum(axis=2)
    NTr_patch = [points_in_each_patch[patch].size for patch in range(len(points_in_each_patch))]
    non_overlapping_patches = [None] * n_patches

    factor_ = 1.0
    if small_dataset:
        points_in_each_smaller_patch = [np.nonzero(dist2[:, index] < ((factor_*eps) ** 2))[0] for index in range(n_patches)]
    else:
        points_in_each_smaller_patch = [np.nonzero(pairwise_distances(x, x[[centers[index]], :], metric='sqeuclidean') < (factor_*eps) ** 2)[0]
                                        for index in range(n_patches)]

    if patch_on_every_point:
        non_overlapping_patches = None
    else:
        for j in range(n_patches):
            list_of_non_overlapping_patches = [] # for patch j
            for j_prime in range(n_patches):
                if j_prime > j: #j_prime != j:
                # if j_prime != j:
                    if not any(np.in1d(points_in_each_smaller_patch[j], points_in_each_smaller_patch[j_prime])):
                        list_of_non_overlapping_patches.append(j_prime)
            non_overlapping_patches[j] = list_of_non_overlapping_patches.copy()

    if patch_on_every_point:
        # overlapping_patches = [[x for x in points_in_each_patch[j] if x > j] for j in range(n_patches)]
        overlapping_patches = [[x for x in points_in_each_patch[j] if x != j] for j in range(n_patches)]
        patch_centers = [centers]
        overlapping_points = None
    else:
        overlapping_patches_and_points = [None] * len(points_in_each_patch)
        for j in range(len(points_in_each_patch)):
            list_of_overlapping_patches = [] # for patch j
            list_of_overlapping_points = []  # for patch j
            list_of_overlapping_local_points_j = [] # for patch j
            list_of_overlapping_local_points_j_prime = [] # for patch j_prime
            list_of_overlapping_points_j_restacked = []  # for patch j
            list_of_overlapping_points_j_prime_restacked = []  # for patch j_prime
            for j_prime in range(len(points_in_each_patch)):
                if j_prime > j: #j_prime != j:
                # if j_prime != j:
                    if any(np.in1d(points_in_each_patch[j], points_in_each_patch[j_prime])):
                        list_of_overlapping_patches.append(j_prime)
                        overlapping_points_orig = points_in_each_patch[j][np.nonzero(np.in1d(points_in_each_patch[j], points_in_each_patch[j_prime]))[0]]
                        list_of_overlapping_points.append(overlapping_points_orig)
                        overlapping_local_points_j = np.array([np.where(points_in_each_patch[j] == i)[0][0] for i in overlapping_points_orig])
                        list_of_overlapping_local_points_j.append(overlapping_local_points_j)
                        overlapping_local_points_j_prime = np.array([np.where(points_in_each_patch[j_prime] == i)[0][0] for i in overlapping_points_orig])
                        list_of_overlapping_local_points_j_prime.append(overlapping_local_points_j_prime)
                        overlapping_points_j_restacked = np.array([np.where(points_in_each_patch[j] == i)[0][0]+sum(NTr_patch[:j]) for i in overlapping_points_orig])
                        list_of_overlapping_points_j_restacked.append(overlapping_points_j_restacked)
                        overlapping_points_j_prime_restacked = np.array([np.where(points_in_each_patch[j_prime] == i)[0][0]+sum(NTr_patch[:j_prime]) for i in overlapping_points_orig])
                        list_of_overlapping_points_j_prime_restacked.append(overlapping_points_j_prime_restacked)
            overlapping_patches_and_points[j] = [list_of_overlapping_patches.copy(),
                                                 list_of_overlapping_points.copy(),
                                                 list_of_overlapping_local_points_j.copy(),
                                                 list_of_overlapping_local_points_j_prime.copy(),
                                                 list_of_overlapping_points_j_restacked.copy(),
                                                 list_of_overlapping_points_j_prime_restacked.copy()]
        overlapping_patches = [overlapping_patches_and_points[j][0] for j in range(len(points_in_each_patch))]
        overlapping_points_j = [overlapping_patches_and_points[j][4] for j in range(len(points_in_each_patch))]
        overlapping_points_j_prime = [overlapping_patches_and_points[j][5] for j in range(len(points_in_each_patch))]
        overlapping_points = [overlapping_points_j, overlapping_points_j_prime]

        kjo = np.array([np.where(points_in_each_patch_orig[indx]==centers[indx])[0][0] for indx in range(n_patches)])
        i_restacked = np.array([int(sum(NTr_patch[:patch]) + kjo[patch]) for patch in range(n_patches)])
        patch_centers = [centers, i_restacked]

    if not patch_on_every_point:
        fig = plt.figure('Patches', figsize=(4, 4))
        if dim == 2:
            if labels is None:
                plt.scatter(x[:, 0], x[:, 1])
            else:
                plt.scatter(x[:, 0], x[:, 1], c=labels, cmap=plt.cm.nipy_spectral)
            plt.scatter(x[centers, 0], x[centers, 1], marker='.', facecolors='none', edgecolors='k')
            patches = [matplotlib.patches.Circle(x[centers[index], :].T, radius=eps, facecolor='none', edgecolor='r') for
                       index in range(len(centers))]
            [fig.gca().add_patch(patches[index]) for index in range(len(centers))]
            for i, txt in enumerate(centers):
                plt.annotate(i, (x[centers[i],0], x[centers[i],1]))
            fig.gca().set_axis_off()

            fig.savefig('patches.png')
            plt.pause(10)
        elif dim == 3:
            ax = fig.add_subplot(111, projection='3d')
            if labels is None:
                ax.scatter(x[:, 0], x[:, 1], x[:, 2])
            else:
                ax.scatter(x[:, 0], x[:, 1], x[:, 2], c=labels, cmap=plt.cm.nipy_spectral)
            u, v = np.mgrid[0:2 * np.pi:50j, 0:np.pi:50j]
            for patch in range(n_patches):
                ax.plot_surface(x[centers[patch],0] + eps * np.cos(u) * np.sin(v),
                                x[centers[patch],1] + eps * np.sin(u) * np.sin(v),
                                x[centers[patch],2] + eps * np.cos(v), color="b", alpha=0.2)
            ax.scatter(x[centers, 0], x[centers, 1], x[centers, 2], marker='o', facecolors='k', edgecolors='k')
            fig.gca().set_axis_off()

            ax.view_init(0,90)

            fig.savefig('patches.png')
            plt.pause(10)
    return points_in_each_patch, patch_centers, overlapping_patches, non_overlapping_patches, overlapping_points, eps


def relabel_patches(points_in_each_patch, consolidate_patches=False, no_overlap=False):
    n_patches = len(points_in_each_patch)
    overlapping_patches_redundant = [None] * n_patches
    for j in range(n_patches):
        list_of_overlapping_patches_redundant = []
        for j_prime in range(n_patches):
            if j_prime != j:
                if any(np.in1d(points_in_each_patch[j], points_in_each_patch[j_prime])):
                    list_of_overlapping_patches_redundant.append(j_prime)
        overlapping_patches_redundant[j] = list_of_overlapping_patches_redundant.copy()
    list_of_lengths = [len(overlapping_patches_redundant[patch]) for patch in range(n_patches)]

    # find first patch
    first_patch_index = list_of_lengths.index(max(list_of_lengths))
    # first_patch_index = list_of_lengths.index(min(list_of_lengths))

    new_unconsolidated_patch_array = []
    new_unconsolidated_patch_array = new_unconsolidated_patch_array + [first_patch_index]
    overlapping_patches_redundant = [list(np.setdiff1d(overlapping_patches_redundant[patch],[first_patch_index]))
                                     for patch in range(n_patches)]

    for each_patch in new_unconsolidated_patch_array:
        if overlapping_patches_redundant[each_patch]:
            patches_to_add = overlapping_patches_redundant[each_patch]
            for each_new_patch in patches_to_add:
                new_unconsolidated_patch_array.append(each_new_patch)
                overlapping_patches_redundant = [
                    list(np.setdiff1d(overlapping_patches_redundant[patch], [each_new_patch]))
                    for patch in range(n_patches)]

    if consolidate_patches:
        points_in_each_patch = [points_in_each_patch[new_unconsolidated_patch_array[idx]] for idx in range(n_patches)]
        overlapping_patches = [None] * n_patches
        for j in range(n_patches):
            list_of_overlapping_patches = []
            for j_prime in range(n_patches):
                if j_prime != j:
                    if any(np.in1d(points_in_each_patch[j], points_in_each_patch[j_prime])):
                        list_of_overlapping_patches.append(j_prime)
            overlapping_patches[j] = list_of_overlapping_patches.copy()

        total = [0] + overlapping_patches[0]
        consolidated_patches = [[0], overlapping_patches[0]]

        for j in consolidated_patches:
            if j != [0]:
                new_entry_consolidated_patches = []
                for j_prime in j: #overlapping_patches[j]:
                    new_entry_consolidated_patches += overlapping_patches[j_prime]
                    new_entry_consolidated_patches = list(np.setdiff1d(list(set(new_entry_consolidated_patches)),total))
                if new_entry_consolidated_patches:
                    consolidated_patches.append(new_entry_consolidated_patches)
                    total += new_entry_consolidated_patches

        # consolidated_patches = consolidated_patches[1:]
        # consolidated_patches[0] = [0] + consolidated_patches[0]

        new_n_patches = len(consolidated_patches)
        logging.info('original nPatches = {0}, new nPatches = {1}'.format(n_patches, new_n_patches))
        points_in_each_consolidated_patch = [None] * new_n_patches
        idx = 0
        logging.info('original points_in_each_patch size = {0}'.format([points_in_each_patch[patch].size for patch in range(n_patches)]))
        for j in consolidated_patches:
            new_entry = points_in_each_patch[j[0]]
            for j_prime in j[1:]:
                new_entry = np.array(list(set(np.append(new_entry,points_in_each_patch[j_prime]))))
            points_in_each_consolidated_patch[idx] = new_entry
            idx += 1
        logging.info('new points_in_each_patch size = {0}'.format([points_in_each_consolidated_patch[patch].size for patch in range(new_n_patches)]))

        if no_overlap:
            cumulative_points = np.copy(points_in_each_consolidated_patch[0])
            for j in range(1,len(consolidated_patches)):
                points_in_each_consolidated_patch[j] = np.setdiff1d(points_in_each_consolidated_patch[j], cumulative_points)
                cumulative_points = np.concatenate((cumulative_points,points_in_each_consolidated_patch[j]))
            logging.info('new points_in_each_patch size = {0}'.format([points_in_each_consolidated_patch[patch].size for patch in range(new_n_patches)]))

        return new_unconsolidated_patch_array, points_in_each_consolidated_patch
    else:
        return new_unconsolidated_patch_array


# def calcFirstPatch():
#
#
