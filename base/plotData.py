import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors


def plotArray(positions, colors, fig=None, titles='', suptitle=None, vmin_vmax_flag=False, vmin_vmax=None):
    fig.clf()
    axes = [[positions[entry][:, 0].min(), positions[entry][:, 0].max(),
             positions[entry][:, 1].min(), positions[entry][:, 1].max(),
             positions[entry][:, 2].min(), positions[entry][:, 2].max()]
            for entry in range(len(positions))]
    cmap = plt.cm.jet
    if vmin_vmax is not None:
        vmin = vmin_vmax[0]
        vmax = vmin_vmax[1]
    elif vmin_vmax_flag:
        vmin = np.amin(np.ravel(colors[0]))
        vmax = np.amax(np.ravel(colors[0]))
    else:
        vmin_all = [np.amin(np.ravel(colors[entry])) for entry in range(len(positions))]
        vmax_all = [np.amax(np.ravel(colors[entry])) for entry in range(len(positions))]
        vmin = np.amin(np.array(vmin_all))
        vmax = np.amax(np.array(vmax_all))
    norm = matplotlib.colors.Normalize(vmin, vmax)
    sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    c = [np.ravel(colors[entry]) for entry in range(len(positions))]

    nPlots = len(positions)
    nCols = 5
    nRows = int(np.ceil(nPlots/nCols))
    ax = []
    for nSubplot in range(nPlots):
        ax += [fig.add_subplot(nRows,nCols,nSubplot+1, projection='3d')]

    delta = 1e-12
    for entry in range(len(positions)):
        ax[entry].set_xlim3d(axes[entry][0]-delta, axes[entry][1]+delta)
        ax[entry].set_ylim3d(axes[entry][2]-delta, axes[entry][3]+delta)
        ax[entry].set_zlim3d(axes[entry][4]-delta, axes[entry][5]+delta)
        ax[entry].scatter(positions[entry][:, 0], positions[entry][:, 1], positions[entry][:, 2], marker='o', s=2, c=cmap(norm(c[entry])))
        ax[entry].set_title(titles[entry])
    if suptitle is not None:
        fig.suptitle(suptitle)
    fig.tight_layout()
    fig.canvas.draw_idle()
    fig.canvas.flush_events()
    return fig