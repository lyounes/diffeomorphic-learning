import logging
import numpy as np
from numba import jit
from scipy.special import logsumexp
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.linear_model import LinearRegression
from sklearn.cross_decomposition import CCA
from sklearn.metrics import pairwise_distances
from sklearn.neighbors import NearestNeighbors


@jit(nopython=True)
def _RidgeSumOfSquares(u,b):
    return (u ** 2).sum()
@jit(nopython=True)
def _RidgeSumOfSquaresGradient(u,b):
    return 2*u, np.zeros(b.shape)

@jit(nopython=True)
def _RidgeSumOfSquaresDiag(u,b):
    return (u ** 2).sum()
@jit(nopython=True)
def _RidgeSumOfSquaresDiagGradient(u,b):
    return 2*u, np.zeros(b.shape)
def _RidgeSumOfSquaresDiagToEye(u,b):
    return ((u - np.ones(u.shape)) ** 2).sum()
@jit(nopython=True)
def _RidgeSumOfSquaresDiagToEyeGradient(u,b):
    return 2*(u - np.ones(u.shape)), np.zeros(b.shape)

@jit(nopython=True)
def _RidgeSumOfSquaresScalar(u,b):
    return b.size * (u ** 2).sum()
@jit(nopython=True)
def _RidgeSumOfSquaresScalarGradient(u,b):
    return b.size * 2*u, np.zeros(b.shape)

@jit(nopython=True)
def _RidgeSumOfSquaresToEye(u,b):
    return ((u - np.eye(u.shape[0], u.shape[1])) ** 2).sum()
@jit(nopython=True)
def _RidgeSumOfSquaresToEyeGradient(u,b):
    return 2*(u - np.eye(u.shape[0], u.shape[1])), np.zeros(b.shape)

@jit(nopython=True)
def _ridgecost(u,b):
    return _RidgeSumOfSquares(u,b)
@jit(nopython=True)
def _ridgecostgradient(u,b):
    return _RidgeSumOfSquaresGradient(u,b)


def _GeneralAffineMult(x, u, b):
    x1 = np.concatenate((np.ones((x.shape[0], 1)), x), axis=1)
    return np.dot(x1, np.concatenate((b,u),axis=0))

def _GeneralAffineBackMult(backState, u, b):
    return np.dot(backState, u.T)

def _GeneralAffineBackPred(forwardState, backpropState):
    forwardStateIn_with_ones = np.concatenate((np.ones((forwardState.shape[0], 1)), forwardState), axis=1)
    ubpred = -np.dot(forwardStateIn_with_ones.T, backpropState)
    return ubpred[1:, :], ubpred[[0], :]

def _DiagAffineMult(x, u, b):
    return _GeneralAffineMult(x, np.diagflat(u), b)

def _DiagAffineBackMult(backState, u, b):
    return _GeneralAffineBackMult(backState, np.diagflat(u), b)

def _DiagAffineBackPred(forwardState, backpropState):
    u_, b_ = _GeneralAffineBackPred(forwardState, backpropState)
    return np.array([np.diag(u_)]), b_

def _ScalarAffineMult(x, u, b):
    return _DiagAffineMult(x, u * np.ones(b.shape), b)

def _ScalarAffineBackMult(backState, u, b):
    return _DiagAffineBackMult(backState, u * np.ones(b.shape), b)

def _ScalarAffineBackPred(forwardState, backpropState):
    u_, b_ = _DiagAffineBackPred(forwardState, backpropState)
    return np.array([[np.sum(u_)]]), b_


def LogisticScore(x, y, w=None, sigmaError=1.0):
    if w is None:
        w = np.ones((x.shape[0], 1))
    res = (np.ravel(w) * (- x[np.arange(x.shape[0])[:, np.newaxis], y].sum(axis=1) + logsumexp(x, axis=1))).sum()
    res = res / (sigmaError ** 2)
    return res


def LogisticScoreGradient(x, y, w=None, sigmaError=1.0):    # called by getGradient
                                                            # returns Del_finalState of LogisticScore
    if w is None:
        w = np.ones((x.shape[0],1))
    gu = np.exp(x)
    pu = gu/gu.sum(axis=1)[:,np.newaxis]
    ni = np.eye(x.shape[1])
    m = np.dot(pu, ni)  # = pu
    grad = (-ni[np.arange(ni.shape[0]), y] + m)*w
    grad = grad / (sigmaError ** 2)
    return grad


def RegressionScore(x, y, sigmaError=1.0):
    res = ((y - x[:,:y.shape[1]]) ** 2).sum()
    res = res / (sigmaError ** 2)
    return res


def RegressionScoreGradient(x, y, sigmaError=1.0):  # called by getGradient
                                                    # returns Del_finalState of RegressionScore
    grad = np.zeros(x.shape)
    grad[:, :y.shape[1]] = -2 * (y - x[:, 0:y.shape[1]])
    grad = grad / (sigmaError ** 2)
    return grad


def RepulseCost_(x, y, sigmaError=1.0, dimensions='all', distance=1.0):
    dist2 = distance ** 2
    t_high = 100.

    if dimensions == 'all':
        dimensions = range(x.shape[1])
    x = x[:, dimensions]
    y = y[:, dimensions]

    res = ((y - x) ** 2).sum()
    res = t_high * np.maximum(0, 1 - res / dist2) ** 2 / (sigmaError ** 2)

    return res


def RepulseCostGradient_(x, y, sigmaError=1.0, dimensions='all', distance=1.0):
    dist2 = distance ** 2
    t_high = 100.
    grad = np.zeros(x.shape)

    if dimensions == 'all':
        dimensions = range(x.shape[1])
    x = x[:, dimensions]
    y = y[:, dimensions]

    res = ((y - x) ** 2).sum()
    grad[:, dimensions] = 4 * t_high * (y-x) * np.maximum(0, 1-res/dist2)/dist2

    grad = grad / (sigmaError ** 2)
    return grad


def RepulseCost(x, y, sigmaError=1.0, dimensions='all', distance=1.0):
    t_low = 1.
    t_high = 100.
    if x.ndim > 2:
        logging.info('\ninput has too many dimensions in RepulseCost function')
        exit()
    dist2 = distance ** 2
    offset = t_low * dist2 / (t_high - t_low)
    alpha = t_high * offset

    if dimensions == 'all':
        dimensions = range(x.shape[1])
    x = x[:, dimensions]
    y = y[:, dimensions]

    if x.ndim == 0:
        res = alpha / (((y - x) ** 2) + offset)
    else:
        res = alpha / (((y - x) ** 2).sum() + offset)

    res = res / (sigmaError ** 2)
    return res

def RepulseCostGradient(x, y, sigmaError=1.0, dimensions='all', distance=1.0):
    t_low = 1.
    t_high = 100.
    if x.ndim > 2:
        logging.info('\ninput has too many dimensions in RepulseCostGradient function')
        exit()
    dist2 = distance ** 2
    grad = np.zeros(x.shape)
    offset = t_low * dist2 / (t_high - t_low)
    alpha = t_high * offset

    if dimensions == 'all':
        dimensions = range(x.shape[1])
    x = x[:, dimensions]
    y = y[:, dimensions]

    grad[:, dimensions] = 2 * alpha * (y - x) / ((((y - x) ** 2).sum() + offset) ** 2)

    grad = grad / (sigmaError ** 2)
    return grad


def FlattenCost(x, y, sigmaError=1.0, dimensions='all', distance=1.0):
    dist2 = distance ** 2
    t_high = 1.

    if dimensions == 'all':
        dimensions = range(x.shape[1])
    x = x[:, dimensions]
    y = y[:, dimensions]

    res = ((y - x) ** 2).sum()
    res = t_high * (res) ** 2 / (sigmaError ** 2)

    return res


def FlattenCostGradient(x, y, sigmaError=1.0, dimensions='all', distance=1.0):
    dist2 = distance ** 2
    t_high = 1.
    grad = np.zeros(x.shape)

    if dimensions == 'all':
        dimensions = range(x.shape[1])
    x = x[:, dimensions]
    y = y[:, dimensions]

    res = ((y - x) ** 2).sum()
    grad[:, dimensions] = - 4 * t_high * (y-x) * res

    grad = grad / (sigmaError ** 2)
    return grad


def ElasticCost(x, y, x0, y0, sigmaError=1.0, dimensions='all'):
    if x.ndim < 2:
        if x.ndim == 1:
            x = x[np.newaxis,:]
            y = y[np.newaxis,:]
        elif x.ndim == 0:
            logging.info('\nfix ElasticCost')
            exit()

    if dimensions == 'all':
        dimensions = range(x.shape[1])
    x = x[:, dimensions]
    y = y[:, dimensions]
    dist2 = ((y0 - x0) ** 2).sum(axis=1, keepdims=True)

    # approach 1:
    alpha = 10.
    res = ((y - x) ** 2).sum(axis=1, keepdims=True)
    res = alpha * ((res/dist2 - 1.0) ** 2).sum() + ((dist2/res - 1.0) ** 2).sum()

    # # approach 2:
    # res = ((y - x) ** 2).sum(axis=1, keepdims=True)
    # res = - np.log(res/dist2) - np.log(-res/dist2 + 2)

    res = res / (sigmaError ** 2)
    return res


def ElasticCostGradient(x, y, x0, y0, sigmaError=1.0, dimensions='all'):
    if x.ndim < 2:
        if x.ndim == 1:
            x = x[np.newaxis,:]
            y = y[np.newaxis,:]
        elif x.ndim == 0:
            logging.info('\nfix ElasticCostGradient')
            exit()

    grad = np.zeros(x.shape)
    if dimensions == 'all':
        dimensions = range(x.shape[1])
    x = x[:, dimensions]
    y = y[:, dimensions]
    dist2 = ((y0 - x0) ** 2).sum(axis=1, keepdims=True)

    # # approach 1:
    alpha = 10.
    res = ((y - x) ** 2).sum(axis=1, keepdims=True)
    grad[:, dimensions] = - 4 * alpha * (res/dist2 - 1) * (y-x)/dist2 + 4 * (dist2/res - 1) * dist2 * (y-x)/(res ** 2)

    # # approach 2:
    # res = ((y - x) ** 2).sum(axis=1, keepdims=True)
    # grad[:, dimensions] = 2*(y-x) * (1/res - 1/(-res + 2*dist2))

    grad = grad / (sigmaError ** 2)
    return grad


def ZeroFlattenCost(x, sigmaError=1.0, dimensions='all'):
    if x.ndim < 2:
        if x.ndim == 1:
            x = x[np.newaxis,:]
        elif x.ndim == 0:
            logging.info('\nfix ZeroFlattenCost')
            exit()

    if dimensions == 'all':
        dimensions = range(x.shape[1])
    x = x[:, dimensions]
    res = (x ** 2).sum()

    res = res / (sigmaError ** 2)
    return res


def ZeroFlattenCostGradient(x, sigmaError=1.0, dimensions='all'):
    if x.ndim < 2:
        if x.ndim == 1:
            x = x[np.newaxis,:]
        elif x.ndim == 0:
            logging.info('\nfix ZeroFlattenCostGradient')
            exit()

    grad = np.zeros(x.shape)
    if dimensions == 'all':
        dimensions = range(x.shape[1])
    x = x[:, dimensions]

    grad[:, dimensions] = 2 * x

    grad = grad / (sigmaError ** 2)
    return grad


def Cost(x, y, sigmaError=1.0, dimensions='all'):
    if x.ndim > 2:
        logging.info('\ninput has too many dimensions in Cost function')
        exit()
    if x.ndim == 0:
        res = (y - x) ** 2
    else:
        if dimensions == 'all':
            dimensions = range(x.shape[1])
        if x.ndim == 1:
            x = x[np.newaxis,:]
            y = y[np.newaxis,:]
        x = x[:, dimensions]
        y = y[:, dimensions]

        res = ((y - x) ** 2).sum()

    res = res / (sigmaError ** 2)
    return res


def CostGradient(x, y, sigmaError=1.0, dimensions='all'):
    if x.ndim > 2:
        logging.info('\ninput has too many dimensions in CostGradient function')
        exit()
    if x.ndim == 0:
        grad = -2 * (y - x)
    else:
        if x.ndim == 1:
            x = x[np.newaxis, :]
            y = y[np.newaxis, :]
        grad = np.zeros(x.shape)
        if dimensions == 'all':
            dimensions = range(x.shape[1])
        x = x[:, dimensions]
        y = y[:, dimensions]
        grad[:, dimensions] = -2 * (y - x)

    grad = grad / (sigmaError ** 2)
    return grad


def RidgeRegularization(u, b, lam=(0.0,), ridgecost=_ridgecost):
    res = 0.0
    nA = len(lam)
    for layer_A in range(nA):
        res += lam[layer_A] * ridgecost[layer_A](u[layer_A], b[layer_A])
    return res


def RidgeGradientInU_(u, b, lam=(0.0,), ridgecostgradient=_ridgecostgradient):
                                                                    # called by getGradient
                                                                    # returns Del_u, Del_b of ridge regularization term
    gradu = [None] * len(lam)
    gradb = [None] * len(lam)
    nA = len(lam)
    for layer_A in range(nA):
        gradu[layer_A], gradb[layer_A] = ridgecostgradient[layer_A](u[layer_A], b[layer_A])
        gradu[layer_A] = lam[layer_A] * gradu[layer_A]
    return gradu, gradb

def RidgeGradientInU(u, b, lam=(0.0,), ridgecostgradient=_ridgecostgradient):
                                                                    # called by getGradient
                                                                    # returns Del_u, Del_b of ridge regularization term
    gradu, gradb = ridgecostgradient(u, b)
    gradu = lam * gradu
    return gradu, gradb


def build3DProjection(X, test=None, Y=None, mode='classification'):
    xTr3 = None
    xTe3 = None
    if Y is None:
        if X.shape[1] < 3:
            xTr3 = np.concatenate((X, np.zeros((X.shape[0], 3 - X.shape[1]))), axis=1)
            if test is not None:
                xTe3 = np.concatenate((test, np.zeros((test.shape[0], 3 - test.shape[1]))), axis=1)
        else:
            xTr3 = np.copy(X)
            if test is not None:
                xTe3 = np.copy(test)
        if X.shape[1] > 3:
            pca = PCA(3)
            xTr3 = pca.fit_transform(xTr3)
            if test is not None:
                xTe3 = pca.transform(test)
    else:
        if mode == 'classification':
            cl = np.unique(Y)
            nc = min(len(cl)-1, 3)
            lda = LDA(n_components=nc)
            xTr3 = lda.fit_transform(X, Y[:,0])
            if test is not None:
                xTe3 = lda.transform(test)
        elif mode == 'regression':
            nc = min(Y.shape[1], 3)
            cca = CCA(n_components=nc)
            xTr3 = cca.fit_transform(X, Y)[0]
            if test is not None:
                xTe3 = cca.transform(test)
        else:
            logging.error('Unknown prodiction mode in build3Dprojection')
            return
        if nc < 3:
            if 3-nc > X.shape[1]:
                xTr3 = np.concatenate((xTr3, np.zeros((xTr3.shape[0], 3-xTr3.shape[1]))), axis=1)
                if test is not None:
                    xTe3 = np.concatenate((xTe3, np.zeros((xTe3.shape[0], 3-xTe3.shape[1]))), axis=1)
            else:
                lin = LinearRegression()
                lin.fit(xTr3, X)
                res = X - lin.predict(xTr3)
                pca = PCA(3 - nc)
                res_ = pca.fit_transform(res)
                xTr3 = np.concatenate((xTr3, res_), axis=1)
                if test is not None:
                    res2 = test - lin.predict(xTe3)
                    res2_ = pca.transform(res2)
                    xTe3 = np.concatenate((xTe3, res2_), axis=1)

    if test is None:
        return xTr3
    else:
        return xTr3, xTe3


def runPCA(X):
    pca = PCA()
    pca.fit_transform(X)
    return np.sqrt(pca.singular_values_)


def read3DVector(filename):
    try:
        with open(filename, 'r') as fn:
            ln0 = fn.readline()
            N = int(ln0[0])
            v = np.zeros([N, 3])
            for i in range(N):
                ln0 = fn.readline().split()
                for k in range(3):
                    v[i, k] = float(ln0[k])
    except IOError:
        print('cannot open ', filename)
        raise
    return v


def loadlmk(filename, dim=3):
    # [x, label] = loadlmk(filename, dim)
    # Loads 3D landmarks from filename in .lmk format.
    # Determines format version from first line in file
    #   if version number indicates scaling and centering, transform coordinates...
    # the optional parameter s in a 3D scaling factor

    try:
        with open(filename, 'r') as fn:
            ln0 = fn.readline()
            versionNum = 1
            versionStrs = ln0.split("-")
            if len(versionStrs) == 2:
                try:
                    versionNum = int(float(versionStrs[1]))
                except:
                    pass

            ln = fn.readline().split()
            N = int(ln[0])
            x = np.zeros([N, dim])
            label = []

            for i in range(N):
                ln = fn.readline()
                label.append(ln)
                ln0 = fn.readline().split()
                for k in range(dim):
                    x[i, k] = float(ln0[k])
            if versionNum >= 6:
                lastLine = ''
                nextToLastLine = ''
                # read the rest of the file
                # the last two lines contain the center and the scale variables
                while 1:
                    thisLine = fn.readline()
                    if not thisLine:
                        break
                    nextToLastLine = lastLine
                    lastLine = thisLine

                centers = nextToLastLine.rstrip('\r\n').split(',')
                scales = lastLine.rstrip('\r\n').split(',')
                if len(scales) == dim and len(centers) == dim:
                    if scales[0].isdigit and scales[1].isdigit and scales[2].isdigit and centers[0].isdigit \
                            and centers[1].isdigit and centers[2].isdigit:
                        x[:, 0] = x[:, 0] * float(scales[0]) + float(centers[0])
                        x[:, 1] = x[:, 1] * float(scales[1]) + float(centers[1])
                        x[:, 2] = x[:, 2] * float(scales[2]) + float(centers[2])

    except IOError:
        print('cannot open ', filename)
        raise
    return x, label


def savelmk(x, filename):
    # savelmk(x, filename)
    # save landmarks in .lmk format.

    with open(filename, 'w') as fn:
        str = 'Landmarks-1.0\n {0: d}\n'.format(x.shape[0])
        fn.write(str)
        for i in range(x.shape[0]):
            str = '"L-{0:d}"\n'.format(i)
            fn.write(str)
            str = ''
            for k in range(x.shape[1]):
                str = str + '{0: f} '.format(x[i, k])
            str = str + '\n'
            fn.write(str)
        fn.write('1 1 \n')


# Reads in .vtk format
def readPoints(fileName):
    with open(fileName, 'r') as fvtkin:
        fvtkin.readline()
        fvtkin.readline()
        fvtkin.readline()
        fvtkin.readline()
        ln = fvtkin.readline().split()
        npt = int(ln[1])
        x = np.array((npt, 3))
        for ll in range(x.shape[0]):
            ln = fvtkin.readline().split()
            x[ll, 0] = float(ln[0])
            x[ll, 1] = float(ln[1])
            x[ll, 2] = float(ln[2])
    return x


# Saves in .vtk format
def savePoints(fileName, x, vector=None, scalars=None):
    if x.shape[1] < 3:
        x = np.concatenate((x, np.zeros((x.shape[0], 3 - x.shape[1]))), axis=1)
    with open(fileName, 'w') as fvtkout:
        fvtkout.write('# vtk DataFile Version 3.0\nSurface Data_old\nASCII\nDATASET UNSTRUCTURED_GRID\n')
        fvtkout.write('\nPOINTS {0: d} float'.format(x.shape[0]))
        for ll in range(x.shape[0]):
            fvtkout.write('\n{0: f} {1: f} {2: f}'.format(x[ll, 0], x[ll, 1], x[ll, 2]))
        if vector is None and scalars is None:
            return
        fvtkout.write(('\nPOINT_DATA {0: d}').format(x.shape[0]))
        if scalars is not None:
            fvtkout.write('\nSCALARS scalars float 1\nLOOKUP_TABLE default')
            for ll in range(x.shape[0]):
                fvtkout.write('\n {0: .5f} '.format(scalars[ll]))

        if vector is not None:
            fvtkout.write('\nVECTORS vector float')
            for ll in range(x.shape[0]):
                fvtkout.write('\n {0: .5f} {1: .5f} {2: .5f}'.format(vector[ll, 0], vector[ll, 1], vector[ll, 2]))

        fvtkout.write('\n')


# Saves in .vtk format
def saveTrajectories(fileName, xt):
    with open(fileName, 'w') as fvtkout:
        fvtkout.write('# vtk DataFile Version 3.0\ncurves \nASCII\nDATASET POLYDATA\n')
        fvtkout.write('\nPOINTS {0: d} float'.format(xt.shape[0] * xt.shape[1]))
        if xt.shape[2] == 2:
            xt = np.concatenate(xt, np.zeros([xt.shape[0], xt.shape[1], 1]))
        for t in range(xt.shape[0]):
            for ll in range(xt.shape[1]):
                fvtkout.write('\n{0: f} {1: f} {2: f}'.format(xt[t, ll, 0], xt[t, ll, 1], xt[t, ll, 2]))
        nlines = (xt.shape[0] - 1) * xt.shape[1]
        fvtkout.write('\nLINES {0:d} {1:d}'.format(nlines, 3 * nlines))
        for t in range(xt.shape[0] - 1):
            for ll in range(xt.shape[1]):
                fvtkout.write('\n2 {0: d} {1: d}'.format(t * xt.shape[1] + ll, (t + 1) * xt.shape[1] + ll))
        fvtkout.write('\n')


def epsilonNet_(x, rate=None, ball_eps=None, num_neighbors=None, small_dataset=True):
    n = x.shape[0]
    dim = x.shape[1]
    inNet = np.zeros(n, dtype=int)
    inNet[0] = 1
    net = np.nonzero(inNet)[0]
    survivors = np.ones(n, dtype=int)
    survivors[0] = 0
    if small_dataset:
        dist2 = ((x.reshape([n, 1, dim]) -
                  x.reshape([1, n, dim])) ** 2).sum(axis=2)
    if ball_eps is not None:
        eps2 = ball_eps ** 2
    elif rate is not None:
        i = np.int_(1.0 / rate)
        if small_dataset:
            d2 = np.sort(dist2, axis=0)
            eps2 = (np.sqrt(d2[i, :]).sum() / n) ** 2
        else:
            logging.info('\nto use rate in patch calculation, set small_dataset=True')
            exit()
    elif num_neighbors is not None:
        nbrs = NearestNeighbors(n_neighbors=num_neighbors, algorithm='ball_tree').fit(x[net,:])

    if small_dataset:
        i1 = np.nonzero(dist2[net, :] < eps2)
    else:
        i1 = np.nonzero(pairwise_distances(x[net,:],x,metric='sqeuclidean') < eps2)
    survivors[i1[1]] = 0
    i2 = np.nonzero(survivors)[0]
    while len(i2) > 0:
        if small_dataset:
            closest = np.unravel_index(np.argmin(dist2[net.reshape([len(net), 1]), i2.reshape([1, len(i2)])].ravel()),
                                       [len(net), len(i2)])
        else:
            closest = np.unravel_index(np.argmin(pairwise_distances(x[net,:],x[i2,:],metric='sqeuclidean').ravel()),
                                       [len(net), len(i2)])
        inNet[i2[closest[1]]] = 1
        net = np.nonzero(inNet)[0]
        if small_dataset:
            # i1 = np.nonzero(dist2[i2[closest[1]],:] < eps2) #
            i1 = np.nonzero(dist2[net, :] < eps2)
        else:
            i1 = np.nonzero(pairwise_distances(x[[i2[closest[1]]],:],x,metric='sqeuclidean') < eps2)
        survivors[i1[1]] = 0
        i2 = np.nonzero(survivors)[0]
    idx = - np.ones(n, dtype=int)
    for p in range(n):
        if small_dataset:
            closest = np.unravel_index(np.argmin(dist2[net, p].ravel()), [len(net), 1])
        else:
            closest = np.unravel_index(np.argmin(pairwise_distances(x[net,:],x[[p],:],metric='sqeuclidean').ravel()), [len(net), 1])
        idx[p] = closest[0]

    return net, idx, np.sqrt(eps2)


def epsilonNet(x, rate=None, ball_eps=None):
    # print 'in epsilon net'
    n = x.shape[0]
    dim = x.shape[1]
    inNet = np.zeros(n, dtype=int)
    inNet[0] = 1
    net = np.nonzero(inNet)[0]
    survivors = np.ones(n, dtype=np.int)
    survivors[0] = 0;
    dist2 = ((x.reshape([n, 1, dim]) -
              x.reshape([1, n, dim])) ** 2).sum(axis=2)
    d2 = np.sort(dist2, axis=0)
    if rate is not None:
        i = np.int_(1.0 / rate)
        eps2 = (np.sqrt(d2[i, :]).sum() / n) ** 2
    elif ball_eps is not None:
        eps2 = ball_eps**2
    # print(n, d2.shape, i, np.sqrt(eps2))

    i1 = np.nonzero(dist2[net, :] < eps2)
    survivors[i1[1]] = 0
    i2 = np.nonzero(survivors)[0]
    while len(i2) > 0:
        closest = np.unravel_index(np.argmin(dist2[net.reshape([len(net), 1]), i2.reshape([1, len(i2)])].ravel()),
                                   [len(net), len(i2)])
        inNet[i2[closest[1]]] = 1
        net = np.nonzero(inNet)[0]
        i1 = np.nonzero(dist2[net, :] < eps2)
        survivors[i1[1]] = 0
        i2 = np.nonzero(survivors)[0]
        # print len(net), len(i2)
    idx = - np.ones(n, dtype=np.int)
    for p in range(n):
        closest = np.unravel_index(np.argmin(dist2[net, p].ravel()), [len(net), 1])
        # print 'p=', p, closest, len(net)
        idx[p] = closest[0]

        # print idx
    return net, idx, np.sqrt(eps2)


def insertZeroColumns(x, indices):
    dims = x.shape[1]+len(indices)
    indices = [indices[idx] if indices[idx] >= 0 else indices[idx]+dims for idx in range(len(indices))]
    for idx in indices:
        x = np.insert(x, idx, 0, axis=1)
    return x
